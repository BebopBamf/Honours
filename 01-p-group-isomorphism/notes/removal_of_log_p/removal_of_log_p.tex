\documentclass[runningheads,a4paper,12pt]{llncs}

\input{preamble}

\addbibresource{p-group-isomorphism.bib}

\title{Removal of Log P from Lemma 3.1 and Lemma 3.2}

\author{}
\institute{}
%\date{June 1, 2023}

\begin{document}

\maketitle

\section{Removing Log P}

\begin{lemma}\label{lem:1}
	Let $L\in\M(t{\times}m,\F_p)$ and $R\in\M(n{\times}t,\F_p)$ be matrices
	where the entries are independently and uniformly sampled from
	$\{0, 1, \ldots, p-1\}$. Let $\delta = p^{-\frac{k^{2}}{4} + (k-1)t}$ for
	positive integers $p, k, t$, then for any matrix $A\in\M(m{\times}n,\F_p)$
	of $\rk~k$, , $LAR$ is a zero matrix with probability at most
	$\frac{1}{\delta}$.
\end{lemma}

\begin{proof}
	Since the probability that $LAR = 0$ is uniform for all $\rk~k$ matrices
	$A \in M(m{\times}n,\F_{p})$ we may assume without loss of generality
	that $A$ is of the form

	\[ A = \begin{bmatrix} I_{k} & 0 \\ 0     & 0 \end{bmatrix} \]
	where $I_{k}$ denotes the $k{\times}k$ identity matrix. Hence, for a
	$t{\times}m$ matrix $L_{1}$ and $n{\times}t$ matrix $R_{1}$, $LAR$ is of the
	form

	\begin{align*}
		LAR & =
		\begin{bmatrix} L_{1} &  \cdots \end{bmatrix}
		\begin{bmatrix}
			I_{k} & 0 \\
			0     & 0
		\end{bmatrix}
		\begin{bmatrix}
			R_{1} \\  \cdots
		\end{bmatrix}                                \\
		    & =
		\begin{bmatrix} L_{1} & \cdots \end{bmatrix}
		\begin{bmatrix} I_{k} \\ 0 \end{bmatrix}
		\begin{bmatrix} I_{k} & 0 \end{bmatrix}
		\begin{bmatrix} R_{1} \\  \cdots \end{bmatrix} \\
		    & =
		\begin{bmatrix} L_{1} &  0 \end{bmatrix}
		\begin{bmatrix} R_{1} \\ 0 \end{bmatrix}       \\
		    & = L_{1}R_{1}
	\end{align*}
	It follows that $LAR=0$ if and only the row space of $L_{1}$ is orthogonal
	to the column space of $R_{1}$ (denoted $L_{1}{\perp}R_{1}$).

	Without loss of generality, we suppose that the row space of $L_{1}$ is
	spanned by a standard basis. Let $\rk(L_{1})=r$ where $r \leq k$. For a
	$r{\times}r$ identity matrix denoted $I_{r}$ and a $r{\times}t$ block
	of the matrix $R_{1}$, denoted $R_{2}$. We can see that

	\begin{align*}
		L_{1}R_{1} & =
		\begin{bmatrix}
			I_{r} & 0 \\
			0     & 0
		\end{bmatrix}
		\begin{bmatrix}
			R_{2} \\
			\cdots
		\end{bmatrix} \\
		           & =
		\begin{bmatrix}
			R_{2} \\
			0
		\end{bmatrix}
	\end{align*}

	Therefore, we get the conditional probability

	\[
		\Pr[L_{1}{\perp}R_{1} \mid \rk(L_{1}) = r] = \Pr[R_{2} = 0]
		= \frac{1}{p^{rt}}
	\]

	Clearly $\Pr[\rk(L_{1}) = 0] = \frac{1}{p^{kt}}$, additionally
	by~\cref{fact:1} we obtain,

	\[
		\Pr[\rk(L_{1}) = r] \leq
		\binom{t}{r} \prod^{r}_{i = 1} (1 - \frac{p^{i}}{p^{k}})
		{(\frac{p^{r}}{p^{k}})}^{k - r}
	\]

	Since the events are independent, it follows that

	\begin{align*}
		\Pr[LAR = 0] & = \Pr[L_{1} \perp R_{1}]                                                           \\
		             & = \Pr[\bigvee^{t}_{r=0} \rk(L_{1}) = r] \Pr[L_{1} \perp R_{1} \mid \rk(L_{1}) = r] \\
		             & = \sum^{t}_{r=0} \Pr[\rk(L_{1}) = r] \Pr[L_{1} \perp R_{1} \mid \rk(L_{1}) = r]    \\
		             & \leq \frac{1}{p^{kt}} + \sum^{t}_{r=1} \binom{t}{r} \prod^{r}_{i=1}
		(1 - \frac{p^{i - 1}}{p^{k}}) {(\frac{p^{r}}{p^{k}})}^{t-r} \frac{1}{p^{rt}}                      \\
		             & = p^{-kt} + \sum^{t}_{r=1} \binom{t}{r} \prod^{r}_{i=1}
		(1 - \frac{p^{i - 1}}{p^{k}}) p^{-r^{2} + kr - kt}                                                \\
		             & \leq p^{-kt} + \sum^{t}_{r=1} \binom{t}{r} p^{-r^{2} + kr - kt}                    \\
		             & = \sum^{t}_{r=0} \binom{t}{r} p^{-r^{2} + kr - kt}                                 \\
		             & \leq \sum^{t}_{r=0} \binom{t}{r} p^{k^{2}/4 - kt}                                  \\
		             & = 2^{t} p^{k^{2}/4 - kt}                                                           \\
		             & \leq p^{k^{2}/4 - kt + t}                                                          \\
		             & = \frac{1}{p^{-k^{2}/4 + (k - 1)t}}
	\end{align*}

	Where the third inequality is a property of quadratic functions over
	$r$\footnote{See Appendix \cref{rem:1}}.
\end{proof}

\begin{theorem}\label{thm:1}
	Let $\mathcal{A}$ be a d-dimensional matrix subspace of
	$\M(m{\times}n, \F_{p})$ for a prime $p$ and positive integers $d,m,n$. For
	any $k \geq 2$, let

	\[
		t := \left\lceil\max\left\{\frac{4d + k^{2}}{4k - 4}, \frac{k}{2}\right\}\right\rceil
	\]

	There is a left individualisation matrix $L \in M(t{\times}m, \F_{p})$ and a
	right individualisation matrix $R \in \M(n{\times}t, \F_{p})$ such that for
	any $A \in \mathcal{A}$ of rank at least $k$, LAR is a non-zero matrix.
\end{theorem}

\begin{proof}
	By union bound and~\cref{lem:1},

	\begin{align*}
		\Pr[\forall A \in \mathcal{A}\text{ of rank at least
		k,}LAR=0] & \leq \sum_{A \in \mathcal{A}\text{ of rank at least
		k}} \Pr[LAR = 0]                                                \\
		          & \leq \frac{p^{d}}{p^{-k^{2}/4 + (k -1)t}}           \\
		          & < 1
	\end{align*}

	where the second inequality is due to the fact that
	$\frac{1}{p^{-k^{2}/4 + (k -1)t}}$ decreases as $k$ increases for all
	$k \leq 2t$ and there are at most $p^{d}$ many matrices of rank at least $k$
	in $\mathcal{A}$.
\end{proof}

\section{Comparison with Lemma 3.1 and 3.2}

The proof utilises linear algebraic techniques to improve upon the original
results presented in Lemma 3.1 and 3.2 the paper ``Faster Isomorphism Testing
for $p$-groups of class 2 exponent $p$''\cite{Sun2023}. We can directly compare
the bounds of the result, both by the parameters achieved and the parameters
that matter to the algorithmic complexity.

\begin{lemma}[Lemma 3.1\cite{Sun2023}]
    Let $A$ be a matrix in $\M(m{\times}n, \F_{p})$ of rank at least $k$ for a
    prime $p$ and some positive integers $m,n,k$. Given an integer
    $1 \leq k' \leq k/2$ and a parameter $0 < \delta < 1$ satisfying
    $\log(1/\delta) \geq k$, let $Q$ be a matrix in $\M(t{\times}k, \F_{p})$
    where
    \[
        t := \left\lceil \frac{8\log(1/\delta)}{k} k' \right\rceil
    \]
    such that the entries of $Q$ are independently and uniformly sampled from
    $\{ 0, 1, \ldots, p - 1 \}$. With probability $1 - \delta$, $QA$ is a matrix
    of rank at least $k'$.
\end{lemma}

\begin{lemma}[Lemma 3.2\cite{Sun2023}]\label{lem:xiaorui-2}
    Let $\mathcal{A}$ be a $d$-dimensional matrix subspace of
    $\M(m{\times}n,\F_{p})$ for a prime $p$ and some positive integers
    $d, m, n$. For any $k \geq 4$, denote
    \[
        t := \left\lceil 32 \max\{d \log p, k\}/\sqrt{k} \right\rceil
    \]
    There is a left individualisation matrix $L \in \M(t{\times}m,\F_{p})$ and a
    right individualisation matrix $R \in \M(n{\times}t,\F_{p})$ such that for
    any $A\in\mathcal{A}$ of rank at least $k$, $LAR$ is a non-zero matrix.
\end{lemma}

In order to compare Xiaorui's result with our result, notice that for Xiaorui's
result~\cref{lem:xiaorui-2}, $t := \Theta(\frac{k}{\sqrt{k}})$ iff
$d \log p < k$, or equivalently, $t := \Theta(\frac{d \log p}{\sqrt{k}})$ iff
$d \log p > k$. Hence, in order to compare the two results we can compare our
result given these two cases.

\newpage
\section*{Appendix}

\begin{fact}\label{fact:1}
	If the entries of a matrix $L$ are independently and uniformly sampled from
	$\{ 1, 2, \ldots, p \}$, the probability that a $t{\times}k$ matrix $L$ has
	$\rk~r$ is at least

	\[
		\binom{t}{r} \prod^{r}_{i = 1} (1 - \frac{p^{i}}{p^{k}})
		{(\frac{p^{r}}{p^{k}})}^{t - r}
	\]
\end{fact}

\begin{proof}
	If a matrix $L$ has $\rk~r$, then by definition the row space is spanned by
	a basis of dimension $r$ denoted
	$\mathcal{B}_{r} = \langle v_{1}, v_{2}, \ldots, v_{r} \rangle$. Let $A_{r}$
	be the event that a vector $v$ is a linear combination of the basis
	$\mathcal{B}_{r}$, given scalars
	$\alpha_{1}, \alpha_{2}, \ldots, \alpha_{r} \in \F_{p}$,
	$v_{i} = \alpha_{1}v_{1} + \alpha_{2}v_{2} + \ldots + \alpha_{r}v_{r}$. The
	probability $\Pr[A_{r}] = \frac{p^{r}}{p^{k}}$ since there are $r$ possible scalars
	and $p^{k}$ possible entries in a vector of size $k$. It follows that the
	probability of the remaining vectors in the row space of $L$ being linear
	combinations of the basis $\mathcal{B}_{r}$ is

	\[
		\Pr[\bigwedge_{t-r} A_{r}] = {(\frac{p^{r}}{p^{k}})}^{t-r}
	\]

	Additionally, let $1 \leq i \leq r$ and $\mathcal{B}_{i}$ denotes the basis
	formed by the first $i$ vectors in $\mathcal{B}_{r}$, then let
	$\overline{A_{i}}$ be the event that a vector $v \in \mathcal{B}_{i}$. It
	follows that with $t$ many choices of vectors to form a basis
	$\mathcal{B}_{r}$ in the row space of size $r$, the probability that $L$ has
	rank $r$ is

	\begin{align*}
		\Pr[\rk(L) = r] & \leq \binom{t}{r} \Pr[\bigwedge_{i \in [r]} \overline{A_{i}} \wedge \bigwedge_{t-r} A_{r}] \\
		                & = \binom{t}{r} \prod^{r}_{i = 1} (1 - \frac{p^{i}}{p^{k}}) {(\frac{p^{r}}{p^{k}})}^{t-r}
	\end{align*}

\end{proof}

\begin{remark}\label{rem:1}
	We can attempt to optimised \cref{fact:1} by considering the cases that
	$t < k$, $t = k$ and $t > k$ but they will yield equivalent results.

	For the case that $t < k$, we use our standard result.

	For the case that $t = k$, it follows that

	\begin{align*}
		\sum^{t}_{r=0} \binom{t}{r} p^{-r^{2} + kr - kt} & = 2^{t} p^{-k^{2} + k^{2} - kt} \\
		                                                 & = 2^{t} p^{-kt}                 \\
		                                                 & \leq p^{t - kt}                 \\
		                                                 & \leq p^{k^{2}/4 + (k - 1)t}
	\end{align*}

	For the case that $t > k$, we can slightly improve \cref{fact:1} since we
	have $k$ many choices of vectors to be linearly independent instead of $t$
	many choices. In this case

	\begin{align*}
		\sum^{t}_{r=0} \binom{k}{r} p^{-r^{2} + kr - kt} & = 2^{k} p^{-r^{2} + kr - kt} \\
		                                                 & \leq 2^{k} p^{k^{2}/4 - kt}  \\
		                                                 & \leq p^{k + k^{2}/4 - kt}    \\
		                                                 & = p^{k^{2}/4 + (1 - t)k}
	\end{align*}
\end{remark}

\newpage
\printbibliography

\end{document}
