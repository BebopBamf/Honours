(TeX-add-style-hook
 "trp-literature-review"
 (lambda ()
   (LaTeX-add-bibitems
    "ArvindEtAl2017"
    "AtkinsonLloyd1981"
    "Babai2016"
    "BabaiEtAl1980"
    "Baer1938"
    "BennettEtAl2001"
    "BrooksbankEtAl2019"
    "CaiEtAl1992"
    "Chen1957"
    "DasEtAl2023"
    "DurEtAl2000"
    "Flanders1962"
    "FutornyEtAl2019"
    "GoldreichEtAl1991"
    "GrochowQiao2021"
    "GrochowQiao2023"
    "GrochowQiao2023a"
    "GroheNeuen2021a"
    "GroheSchweitzer2020"
    "IvanyosQiao2019"
    "Karp1972"
    "KoblerEtAl1993"
    "LiQiao2017"
    "McKay1981"
    "McKayPiperno2014"
    "Miller1978"
    "OBrien1994"
    "PfefferEtAl2019"
    "Rosenbaum2013"
    "Sun2023"
    "Sun2023a"
    "TangEtAl2022"
    "XuEtAl2019"
    "zotero-82"))
 '(or :bibtex :latex))

