(TeX-add-style-hook
 "index-slim"
 (lambda ()
   (LaTeX-add-labels
    "def:algebraic-isomorphism-problem"
    "alg:brute-force"))
 :latex)

