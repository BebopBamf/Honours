(TeX-add-style-hook
 "index"
 (lambda ()
   (LaTeX-add-labels
    "def:algebraic-isomorphism-problem"
    "alg:brute-force"))
 :latex)

