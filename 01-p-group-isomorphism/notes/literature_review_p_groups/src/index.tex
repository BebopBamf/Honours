\section{Introduction}

\textit{Isomorphism Testing of Algebraic and Combinatorial Structures} asks to
decide whether two objects are \textit{isomorphic}. Understanding the complexity
is to try and improve the worst-case asymtoptic time-complexity on a
deterministic turing machine. An isomorphism ($\cong$) is an equivalence relation
that is less strict than an equality. This is important to all fields
of mathematics and science since very rarely real world objects are strictly
equivalent (for example trying to match objects given by a image representation,
or match molecules given their graphs).

The \textit{Isomorphism Testing Problem} for algebras and combinatorial
structures is a widely studied problem in theoretical computer science \cite{GrochowQiao2023a, GrochowQiao2021, GrochowQiao2023}. It has a variety of
applications in quantum information, post-quantum cryptography and machine
learning, with the most widely studied problem being that of Graph Isomorphism.

Isomorphism problems are studied heavily in the field of computational
complexity. Graph Isomorphism was one of the first problems described in Karp's
seminal paper describing the 21 NP-complete problem~\cite{Karp1972}. In which
Karp listed Graph Isomorphism as an open question whether Graph Isomorphism is
in P or is NP-complete. Graph Isomorphism is the most studied Isomorphism
Problem, however only recently graph isomorphism was shown to be
quasi-polynomial time~\cite{Babai2016}. While theoretically graph isomorphism
seems like a difficult problem, in practice it is quite easy, for example
Babai, Erdos and Selkow showed Graph Isomorphism has a linear time average case
running time~\cite{BabaiEtAl1980}. Graph Isomorphism is seen as effectively
solved in practice, see \cite{McKay1981, McKayPiperno2014}.

Graph Isomorphism is important as many objects can be generalised as graphs,
however showing that graph isomorphism can be solved efficiently for all graphs
(this is equivalent to showing graph isomorphism is in P). It is thought by
experts in the field that we first need to improve group isomorphism
\cite{Babai2016}.

Compared to Graph Isomorphism Problem, the isomorphism problems for general
algebraic structures such as tensors, and polynomials are much more difficult to
solve. Like Graph Isomorphism they also exhibit the property that it is unknown
whether they are in P or are NP-complete. Compared to Graph Isomorphism which
took many years to develop a quasi-polynomial time algorithm. The \textit{Group
  Isomorphism Problem} is an interesting case. It has been quasi-polynomial time
bounded since its initial investigation, with time $N^{O(\log N)}$ where $N$
denotes the group order, originally attributed to Tarjan, \cite{Miller1978}.
With the best known improvement not improving Group Isomorphism testing to even
$N^{o(\log N)}$, \cite{Rosenbaum2013}. The \textit{group isomorphism} problem
asks to decide whether two finite groups given by their cayley tables are
isomorphic. This is at least as hard as graph isomorphism since group
isomorphism can be cast as a graph isomorphism problem, \cite{KoblerEtAl1993}.
However, there has been relatively little progress in improving the worst-case
bound for Group Isomorphism compared to Graph Isomorphism.

A particularly interesting and hard case of group isomorphism testing is the
isomorphism testing problem of $p$-groups of (nilpotent) class 2 and exponent
$p$. Historically this class proved difficult to show an improvement over the
current status quo of group isomorphism with running time $N^{O(\log N)}$, until
Sun showed $p$-groups of class 2 and exponent $p$ could be tested in time
$N^{o(\log N)}$, \cite{Sun2023a}.

\subsection{The Practical Applications of Isomorphism Testing}

The isomorphism testing of algebraic structures, specifically of polynomials,
tensors and groups has been widely studied for applications in machine learning,
quantum information, and post-quantum cryptography.

\textit{Computational Group Theory} is a branch of applied mathematics concerned
with developing efficient algorithms relating to group theory, see
(\cite{OBrien1994}). A notoriously difficult case of groups for computation is
$p$-groups of class 2 and exponent $p$. Currently little improvement has been
made over the previous results however the representation of $p$-groups as
linear spaces of matrices presents new avenues to explore within computational
group theory, \cite{LiQiao2017}.

In \textit{Post-Quantum Cryptography}, Graph Isomorphism had been explored as a
potential avenue as a protocol for zero-knowledge proofs, see
\cite{GoldreichEtAl1991}. However due to Graph Isomorphism being easily
solvable in practice \cite{BabaiEtAl1980}, Graph Isomorphism did not provide a
solid foundation as a Cryptography Scheme. In contrast with Graph Isomorphism,
the Tensor Isomorphism Problem seems much more difficult, difficult enough
practically to yield a plausible cryptography scheme, see \cite{TangEtAl2022}.

In \textit{Quantum Information} an important question asking if two quantum
states are interconvertible by SLOCC (local operation and classical
communication statistically). This can be cast as a tensor
isomorphism testing problem, \cite{DurEtAl2000, BennettEtAl2001}.

Within \textit{Data Science and Machine Learning} in feature extraction a method
would be to utilise a \textit{signature tensor} and reconstruct the path of the
tensor, see \cite{Chen1957}. This can be seen as a tensor congruence problem,
see \cite{PfefferEtAl2019}.

\section{Critical Evaluation of Sources}

\subsection{Sun's Algorithm for $p$-group isomorphism testing}

\begin{table}[H]
	\centering
	\begin{tabular}{ l l }
		Title     & Faster Isomorphism for $p$-groups of (Nilpotent) Class 2 and
		Exponent $p$                                                             \\
		Author    & Xiaorui Sun                                                  \\
		Topics    & Isomorphism Testing, Computational Complexity, Computational
		Group Theory                                                             \\
		Published & Preprint ArXiV'2023, Conference STOC'2023                    \\
	\end{tabular}
\end{table}

Sun presented a reduction from skew-symmetric matrix space isometry to
skew-symmetric matrix tuple isometry in \cite{Sun2023a}. His reduction showed
due to Baer's correspondence that $p$-group isomorphism could be tested using
the polynomial time algorithm presented in \cite{IvanyosQiao2019}. In his
result he managed to improve upon a result which was a long standing road block
in the field of computational group theory. His reduction was inspired partially
by linear algebraic analogues to graph isomorphism techniques, see
\cite{LiQiao2017}.

The \textit{relevence} of Sun's paper is incredibly important to isomorphism
testing. Isomorphism testing of $p$-groups of nilpotent class 2 and exponent $p$
has been a widely referenced difficult case that is a roadblock to many other
isomorphism testing problems \cite{Babai2016, GrochowQiao2021, GrochowQiao2023,
	GrochowQiao2023a}.

Sun's paper is very \textit{reliable} because it has been peer reviewed by
experts in the field. It has been published in STOC'2023 a very reputable
computer science conference.

Sun's paper is \textit{accurate} since his proofs have been peer reviewed and
verified, his results have been examined carefully so the chance of a mistake is
low. His methodology is well-known, in the preprint he describes in detail his
results \cite{Sun2023}.

Sun has \textit{no potential for bias} into his results because they are
mathematically proven and verified.

His result is incredibly \textit{timely} as his result was only published this
year. His result is both the current state of the art, and builds upon previous
research.

His results allow for possible improvements, through tightening the bounds on
some of his results or for expanding his result of testing \textit{odd}
$p$-groups of class 2 exponent $p$ for a prime power $p$, to that of
\textit{all} $p$-groups of class 2 exponent $p$ (or potentially other algebras).

This makes his research a good potential candidate for contribution in the field
of computational complexity and isomorphism testing. It is very feasible to
produce a result in a reasonable amount of time.

\subsection{Grochow and Qiao on the Complexity of Isomorphism Problems for
	Tensors, Groups and Polynomials}

\begin{table}[h!]
	\centering
	\begin{tabular}{ l l }
		Title     & On the Complexity of Isomorphism Problems for Tensors, Groups
		and Polynomials I:                                                         \\
		          & Tensor Isomorphism Completeness                                \\
		Author    & Joshua Grochow \& Youming Qiao                                 \\
		Topics    & Isomorphism Testing, Computational Complexity, Computational
		Group Theory                                                               \\
		Published & Preprint arXiv'2019, Conference ITCS'2021, Journal SICOMP'2023 \\
	\end{tabular}
\end{table}

Grochow and Qiao introduced a series of papers describing the complexity class
TI which is defined as the problems polynomial-time Turing (Cook) reducible to
the $d$-Tensor Isomorphism Problem. This paper is very interesting because it
encapsulates the complexity of many related isomorphism testing problems of
algebraic structures, including that of the case of $p$-groups of class 2
exponent $p$.

Grochow and Qiao present an incredibly \textit{relevant} paper. This paper
effectively encapsulates isomorphism testing to a single complexity class. This
allows us to look at isomorphism testing as a singular entity rather than as
multiple related problems.

Additionally the paper is clearly \textit{reliable}. It has 3 distinct versions,
including peer-reviewed journal and conference publications. ITCS and SICOMP are
both respectively reputable publications.

This paper is \textit{accurate} and has \textit{no potential for bias} since
it has been known for 4 years before publication and checked multiple times. As
well as being a result only indicating the mathematical relationship with all
problems through proof techniques.

This paper establishes a new research area, where all problems are examined
through a group acting on a linear structure. This means the result is
\textit{timely} as it is both recent and central to the entire topic.

The paper was never intended to be \textit{complete}. In fact since the initial
publication many papers in the series have been published
\cite{GrochowQiao2023a, GrochowQiao2021, GrochowQiao2023}. Since the
establishment of the TI complexity class, every time a TI-complete problem
is improved it changes the framework of the entire problem space yielding a
requirement for reanalysis.

Essentially this paper demonstrates a tight relationship between isomorphism
testing problems and results in any new technique for testing a TI-complete
problem yielding new bounds for the other related problems. This is clearly seen
by the latest paper in the series where they examine how Sun's algorithm extends
to other isomorphism testing problems \cite{GrochowQiao2023}.

\section{Literature Review}

The isomorphism testing problem for algebraic structures given by a group acting
on a set are closely related problems which can be described by what the
underlying set is. A group acting on combinatorial structures (graphs) have been
heavily explored and researched. Trivially yielding the successful
Weisfeiler-Leman algorithm and colour refinement \cite{BabaiEtAl1980}. However
it is also important to consider a group acting on a linear structure. Finite
groups can be represented as matrix spaces and lie algebras, and polynomials can
also trivially be represented as (multi)linear structures. This is the main
motivation of the complexity class TI.

\subsection{Preliminary Definitions and Notation}

First we describe some basic definitions and notation.

\textit{Sylow $p$-groups and Nilpotent Groups}. A \textit{commutator} of a group
$G$ is defined as $[a,b] = a^{-1}b^{-1}ab$, if $A, B$ are subgroups of $G$ than the
\textit{commutator subgroup} is defined as $[A,B]$ is the group generated by
$[a,b]$ where $a \in A, b \in B$. The \textit{lower central series} of a group $G$ is
defined as $\gamma_{1} = G, \gamma_{k+1}(G) = [\gamma_{k}(G), G]$. A group is defined to be
\textit{nilpotent} if there is a $c$ such that $\gamma_{c+1}(G) = 1$. So $c$ denotes
the class of the group. A classic result in finite group theory is that for a
finite group, the group is nilpotent if and only if it is the product of it's
Sylow subgroups. So the groups of prime power order $p^{r}$ are nilpotent.

\textit{Linear spaces}. $\mathbb{F}_{q}$ denotes a finite field of order $q$.
Sometimes written as $\mathbb{F}_{p}$ depending on the context. The
corresponding linear space of dimension $n$ over the field is denoted as
$\mathbb{F}^{n}_{q}$.

\textit{Matrices and Matrix spaces}. The space of $n{\times}n$ matrices over the
field $\mathbb{F}_{q}$ is denoted $M(n, \mathbb{F}_{q})$ and the space of
$n{\times}m$ matrices over $\mathbb{F}_{q}$ is similarly denoted
$M(n{\times}m, \mathbb{F}_{q})$. Given a matrix $A \in M(n{\times}m, \mathbb{F}_{q})$, the
transpose of $A$ is denoted $A^{\top}$. A matrix $A \in M(n{\times}m,\mathbb{F}_{q})$ is
said to be \textit{symmetric} if $A = A^{\top}$ and \textit{skew-symmetric} or
\textit{alternating} if $A = -A^{\top}$. The space of alternating matrices is
denoted $\Lambda(n, \mathbb{F}_{p})$.

\textit{Linear Groups}. $GL(n, \mathbb{F}_{q})$ denotes the general linear
group or the set of $n{\times}n$ invertible matrices over the field $\mathbb{F}_{q}$.

\begin{table}[h!]
	\centering
	\begin{tabular}{ l l l }
		Font                               & Object         & Corresponding space
		\\
		\hline
		$A,B,\ldots$                       & matrix         & $M(n, \mathbb{F}_{q})$ or
		$M(n \times m, \mathbb{F}_{q})$                                                                  \\
		$\mathcal{A},\mathcal{B},\ldots$   & matrix  spaces & The subspaces of $M(n, \mathbb{F}_{q})$ or
		$M(n{\times}m, \mathbb{F}_{q})$
		\\
		$\mathfrak{A},\mathfrak{B},\ldots$ & matrix  spaces & The subspaces of $M(n, \mathbb{F}_{q})$ or
		$M(n{\times}m, \mathbb{F}_{q})$ corresponding to a tensor slice
		\\
		$\mathtt{A},\mathtt{B},\ldots$     & tensors        &
		$T(n{\times}m{\times}l, \mathbb{F}_{q})$                                                         \\
	\end{tabular}
	\caption*{Description of notation and corresponding objects.}
\end{table}

Here we give a general definition, that extends to all (algebraic \&
combinatorial) structures.

\begin{definition}\label{def:algebraic-isomorphism-problem}
	The Isomorphism Testing Decision Problem for Algebraic Structures can be
	generalised as Given a group $G$ acting on a set $X$, decide if two set
	elements $x,y \in X$ are in the same $G$-orbit.
\end{definition}

An important distinction between the isomorphism testing of graphs and the
isomorphism testing of algebraic structures. In the testing of graphs the
underlying set that the group acts on is a combinatorial structure (implying the
use of combinatorial techniques to solve graph isomorphism). While the group
acting on an algebra is generally linear, and the underlying set being a
(multi)-linear structure (implying the use of linear algebraic techniques).

Tensors can be understood as multidimensional arrays, and 3-Tensors which denote
3-dimensional arrays can be understood as matrix spaces when slicing them. This
leads to important definitions of actions on matrix spaces.

\begin{definition}
	Two matrix spaces $\mathcal{A}, \mathcal{B} \subseteq M(n{\times}m, \mathbb{F}_{p})$ are said to be
	\textit{equivalent} if (and only if) there exists matrices
	$P \in GL(m, \mathbb{F}_{p})$ and $Q \in GL(n, \mathbb{F}_{p})$ such that
	$P\mathcal{A}Q = \mathcal{B}$, and $P\mathcal{A}Q$ denotes the space $\{ PAQ \mid A \in \mathcal{A} \}$. Similarly two
	matrix spaces $A, B \subseteq M(n, \mathbb{F}_{p})$ are said to be
	\textit{conjugate} if there exists a $P \in GL(n, \mathbb{F}_{p})$ such that
	$P\mathcal{A}P^{-1} = \mathcal{B}$. Finally the matrix spaces are said to be \textit{isometric}
	if and only if $P\mathcal{A}P^{\top} = \mathcal{B}$.
\end{definition}

\subsection{Tensor Isomorphism and Tensor Isomorphism Completeness}

Given that algebraic isomorphism problems seem harder to improve at least in
literature compared to that of graph isomorphism, it is important to understand
the \textit{theoretical complexity} of algebraic isomorphism problems. Algebraic
Isomorphism Problems can trivially be understood in the complexity class GI
which is explored by Kobler in his book on Graph Isomorphism Complexity
(\cite{KoblerEtAl1993}). However, this approach has limitations since while
Graph Isomorphism is easy in practice, algebraic isomorphism problems are
thought to be intractable (\cite{GrochowQiao2023}). It is unknown
whether algebraic isomorphism problems are P or NP-complete either so the usual
framework for dealing with intractability is not very useful. That is to say, we
need to come up with a way of understanding the complexity of algebraic
isomorphism problems that properly encapsulates the difficulty of them.

This is the primary motivation for the development of the complexity class TI
and the definition of TI-complete which is the hardest problems in TI.

Recall definition~\ref{def:algebraic-isomorphism-problem}. This can be extended
to the $d$-Tensor Isomorphism Problem where the general linear group acts on the
sides of the tensor.

\begin{definition}
	For vector spaces $V_{i} \in \mathbb{F}^{n_{i}}_{q}$ over a field
	$\mathbb{F}_{q}$. A $d$-Tensor $\mathtt{T} = V_{1} \otimes V_{2} \otimes \ldots \otimes V_{d}$ and
	$\mathtt{S} = U_{1} \otimes U_{2} \otimes \ldots \otimes U_{d}$. The $d$-Tensor Isomorphism Problem
	asks to decide whether there exists matrices
	$A_{i} \in GL(n_{i}, \mathbb{F}_{q})$ such that for a tensor $\mathtt{T}$,
	$A_{i}$ acting on $V_{i}$ produces $\mathtt{S}$. Trivially the $3$-Tensor
	Isomorphism Problem is equivalent with three vector spaces $U, V, W$ and the
	tensors $U \otimes V \otimes W$.
\end{definition}

This leads us to the related notion of the complexity class TI and
TI-completeness. Like graph isomorphism, the class TI gives a notion of which
problems exhibit similar properties, such as being somewhere between NP-complete
and P. Showing that a variety of problems were in TI including isomorphism for
algebras over a finite field was a classic result, see \cite{FutornyEtAl2019}.
However, showing that problems in TI exhibit the same property of intractability
required introducing the notion of TI-complete problems, this is the major
result of Grochow and Qiao \cite{GrochowQiao2023}.

\begin{definition}[\cite{GrochowQiao2023}]
	TI or TI$\mathbb{F}_{q}$ is the class of problems polynomial-time turing
	(Cook) reducible to the $d$-tensor isomorphism problem for a fixed $d$ over a
	field $\mathbb{F}_{q}$. A problem is said to be \textit{TI-hard} iff for any
	$d$, $d$-Tensor Isomorphism reduces to the problem in polynomial time
	(turing). Finally a problem is said to be \textit{TI-complete} iff it is both
	TI-hard and in TI. Note that TI-completeness denotes the hardest problems in TI.
\end{definition}

The significance of this cannot be understated. Showing that problems are
TI-complete is fundamental to understanding the complexity of problems such as
group isomorphism (for which the hardest cases are TI-complete) and as evidence
for showing that Graph Isomorphism is in P. That is to say if we show any
problem that is TI-complete can be solved efficiently, we improve the bounds for
all TI-complete problems, the problem of $p$-groups is specifically of interest
due to Babai \cite{Babai2016} describing them as a roadblock to showing GI is
in P. This leads to an important theorem in the testing of algebraic isomorphism
problems.

\begin{theorem}[\cite{GrochowQiao2023}]
	The following problems are TI-complete.

	\begin{enumerate}
		\item
		      \textbf{3-Tensor Isomorphism} (over a field $\mathbb{F}_q$)
		\item
		      \textbf{Group Isomorphism} for the case of $p$-groups of (nilpotent)
		      class 2 (reduces to TI$\mathbb{F}_{p^{c}}$ for the special case of
		      tensors over the field $\mathbb{F}_{p^{c}}$).

		\item
		      \textbf{Matrix Space Isometry}

		\item
		      \textbf{Matrix Space Conjugacy}

		\item
		      \textbf{Algebra Isomorphism} including the cases:

		      \begin{enumerate}
			      \item
			            \textbf{Associative Algebra Isomorphism} for algebras that
			            commutative and unital, or for algebras that are commutative
			            and 3-nilpotent.

			      \item
			            \textbf{Lie Algebra Isomorphism} for 2-step nilpotent Lie
			            Algebras

		      \end{enumerate}

		\item
		      \textbf{Cubic Form Equivalence} and \textbf{Trilinear Form Equivalence}.

	\end{enumerate}
\end{theorem}

\subsection{Isomorphism Testing for $p$-Groups of Class 2 and Exponent $p$}

Graph Isomorphism has been incredibly important to the field of machine
learning \cite{zotero-62}. Interestingly, while Graph Isomorphism is not known
to be in P, Graph Isomorphism is efficiently solvable in practice due to the
successful weisfeiler-leman and colour refinement techniques, see
\cite{CaiEtAl1992}. In fact this algorithm forms the basis of graph neural
networks and is shown to be as powerful as graph isomorphism networks, see
\cite{XuEtAl2019}.

Tensors and linear structures are much easier for a computer to represent
however, it has long been known that it is much more difficult to efficiently
compute TI-complete problems (specifically that of $p$-groups class 2 exponent
$p$) in practice. Colour refinement and weisfeiler-leman (colour refinement is a
special case of weisfeiler-leman) have been successful in the average case
analysis of graph isomorphism, see \cite{BabaiEtAl1980}. This leads to the
question of applying a refinement technique in the context of linear group
actions. This lead to the linear analogues as a technique for the average case
complexity analysis of tensor isomorphism, see \cite{LiQiao2017,
	BrooksbankEtAl2019}.

However in the context of worst-case analysis it is important to understand the
complexity of $p$-group of class 2 exponent $p$ isomorphism since as per the
introduction it is the most well-known TI-complete problem in literature.
Importantly $p$-group of class 2 exponent $p$ under Baer's correspondence can be
represented as alternating matrix spaces, and again represented as a 3-tensor by
combining the matrix spaces to form 3-dimensional arrays.

\begin{lemma}[\cite{Baer1938}]
	Let $G,H$ be two $p$-groups of class 2 and exponent $p$, with group order
	$p^{c}$. There is a bijection between $G$ and $H$ and corresponding
	alternating bilinear maps $G/Z(G) \times G/Z(G) \to [G,G]$ where $Z(G)$ denotes the
	center of the group. Since bilinear maps can be represented as alternating
	matrix spaces $\mathcal{G}, \mathcal{H} \subseteq \Lambda(n, \mathbb{F}_{p})$. The isomorphism problem for
	$p$-groups of class 2 exponent $p$ is equivalent to the matrix space isometry
	problem over the field $\mathbb{F}_{p}$. Which is decide if there exists a
	matrix $P \in GL(n, \mathbb{F}_{p})$ such that $P\mathcal{A}P^{\top} = \mathcal{B}$.
\end{lemma}

Now we can ask what is the worst case complexity of $p$-groups of class 2
exponent $p$ given a brute force algorithm.

\begin{lemma}
	There exists a brute-force algorithm for testing $p$-groups of class 2 and
	exponent $p$ in time $N^{O(\log N)}$ where $N = p^{c}$ denotes the group
	order.
\end{lemma}

\begin{proof}
	Consider the algorithm.

	\begin{algorithm}\label{alg:brute-force}
		\caption{Brute-Force Algorithm for p-group of class 2 exponent p}.
		\begin{algorithmic}
			\Input
			\Desc{$\mathcal{A}$}{A matrix spaces in $\Lambda(n,\mathbb{F}_{p})$ which is a
				representations of $p$-groups of class 2 exponent $p$}
			\Desc{$\mathcal{B}$}{A matrix spaces in $\Lambda(n,\mathbb{F}_{p})$ which is a
				representations of $p$-groups of class 2 exponent $p$}
			\EndInput
			\Output
			\Desc{YES}{Is isomorphic}
			\Desc{NO}{Not isomorphic}
			\EndOutput

			\textbf{Algorithm}

			\begin{enumerate}
				\item
				      For each matrix in $P \in GL(n, \mathbb{F}_{p})$, if $P\mathcal{A}P^{\top} = \mathcal{B}$,
				      return YES.

				\item
				      Return NO.

			\end{enumerate}
		\end{algorithmic}
	\end{algorithm}

	Since their are $p^{n^{2}}$ possible entries for a matrix in
	$M(n, \mathbb{F}_{p})$ the running time of Alg 1.
	$\leq p^{n^{2}} \cdot \text{poly}(n,\log p)$. So clearly the algorithm is
	in time $N^{O(\log N)}$.
\end{proof}

The point of this shows that even a naive brute force algorithm is
quasi-polynomial time bounded. As described before, linear analogues to colour
refinement and weisfeiler-leman have been successful in average case analysis of
this problem \cite{LiQiao2017,BrooksbankEtAl2019}. Inspired by this Sun
introduces two techniques inspired by colour refinement approaches called
\textit{matrix space indvidualisation refinement} and \textit{low-rank matrix
  characterisation}, \cite{Sun2023a}. In matrix space individualisation
refinement, Sun presents a proof that we can find a left and right
multiplication matrix with a resulting image of matrices of smaller size, such
that given a suitably high rank, they produce a non-zero matrix. However, while
the left and right matrices are small enough to enumerate with little cost to
the algorithm runtime, it does not deal with small rank matrices. Based on the
work of Flanders, Atkinson and Lloyd, Sun introduces a method called \textit{low
rank matrix characterisation} for producing smaller matrices from low rank
matrices \cite{Flanders1962, AtkinsonLloyd1981}.

\begin{theorem}[\cite{Sun2023a}]
  Let $\mathcal{A}$ and $\mathcal{B}$ be two $n{\times}n$ skew-symmetric matrix spaces, both of dimension
  $m$, over $\mathbb{F}_{p}$ for some prime number $p > 2$ and positive $m, n$.
  There is an algorithm with running time $p^{O((n + m)^{1.8} \cdot \log p)}$, which
  determines whether there exists a matrix $S \in GL(n, \mathbb{F}_{p})$ such that
  $S\mathcal{A}S^{\top} = \mathcal{B}$.
\end{theorem}

This presents the secondary result from Sun which shows that skew-symmetric
matrix space isometry testing can be improved upon.

\begin{theorem}[\cite{Sun2023a}]
  Let $G$ and $H$ be two $p$-groups of class 2 exponent $p$ for an odd prime
  power $p$ (where the group order is $p^{c}$), there is an algorithm with
  running time $N^{O((\log N)^{5/6})}$ to determine whether $G$ and $H$ are
  isomorphic, where $N$ denotes the group order.
\end{theorem}

This is Sun's main result, it shows clearly that $p$-groups of class 2 and
exponent $p$ are testable in time $N^{o(\log N)}$. Using new techniques he
improves a long standing bound in isomorphism testing.

\subsection{Future Work and Review}

We have new tools for testing matrix space isometry. However, there is still an
important class of $p$-groups which Sun explicitly did not test for, that of
$2$-groups. Additionally, whether his lower bounds are optimal is still a open
question to experts. It is currently under investigation whether his bounds can
be improved.

The big question also remains about what his breakthrough means for all
TI-complete problems. That is a question first answered by Grochow and Qiao in
their recent paper \cite{GrochowQiao2023}. However, given the potential
improvements of the bounds of $p$-group isomorphism, very soon the bounds of all
TI-complete problem may once again require investigation.

\section{Conclusion}

The important problem of the isomorphism testing of TI-complete problems has the
potential to revolutionalise many industries. As described it could potentially
introduce new powerful techniques of machine learning \cite{XuEtAl2019}.

Recently the cryptography scheme based on alternating trilinear form equivalence
was proposed as a NIST submission for the post-quantum cryptography
standardisation process (ALTEQ), see appendix ALTEQ. Understanding the
computational complexity of the TI-complete problems is fundamentally important
to the success of the submission.

In conclusion the TI-complete problems are still not well understood, there is
still much discovery to be made about how much they can be improved.
Understanding the complexity of such problems can lead to new techniques and
improvement to long standing questions in mathematics that have formed the
foundation of techniques prevalent to those in machine learning. As quantum
computing and machine learning become ever prevalent in industry understanding
how complex these problems are become ever important to pushing the bounds of
the future of computing.
