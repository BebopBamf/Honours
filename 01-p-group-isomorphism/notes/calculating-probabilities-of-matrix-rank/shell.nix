{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/40f79f003b6377bd2f4ed4027dde1f8f922995dd.tar.gz") {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.biber
  ];
}

