clf;
figure(1);
format compact 
h(1) = axes('Position',[0.2 0.2 0.6 0.6]);

v1 = [1 1 -1];
v2 = [-1 1 -1];
v3 = [-1 1 1];
v4 = [1 1 1];
v5 = [-1 -1 1];
v6 = [1 -1 1];
v7 = [1 -1 -1];
v8 = [-1 -1 -1];

vert = [1 1 -1;
        -1 1 -1;
        -1 1 1;
        1 1 1;
       -1 -1 1;
       1 -1 1;
    1 -1 -1;
    -1 -1 -1];

fac = [1 2 3 4; 
       4 3 5 6; 
       6 7 8 5; 
       1 2 8 7; 
       6 7 1 4; 
       2 3 5 8];

% I defined a new cube whose length is 1 and centers at the origin.
vert2 = [0 1 -1; % checked
         -1 1 -1; 
         -1 1 0;
         0 1 0;
         -1 0 0;
         0 0 0;
         0 0 -1;
         -1 0 -1];
fac2 = fac;

patch('Faces',fac,'Vertices',vert,'FaceColor','b');  % patch function
axis([-2, 2, -2, 2, -2, 2]);
axis equal;
xlabel('x');
ylabel('y');
zlabel('z');

hold on;

patch('Faces', fac2, 'Vertices', vert2, 'FaceColor', 'r');
material metal;
alpha('color');
alphamap('rampdown');
view(3);
