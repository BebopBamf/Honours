% In this section, we apply the dynamic programming technique of Luks \cite{Luks99} to obtain a
% deterministic algorithm for $\AltMatSpIso$ in time $q^{\frac{1}{4}(n^2+m^2)+o(n^2+m^2)}$. Remarkably, this 
% algorithm indicates that testing the isomorphism of $p$-group of nilpotent class $2$ and exponent $p$ can be done
% in time $q^{\frac{1}{4}(n^2+m^2)+o(n^2+m^2)}$. Before this, the best known algorithm for this type
% of groups is introduced by Rosenbaum \cite{Ros13b}, which runs in time 
%$q^{\frac{1}{4}(n+m)^2+o((n+m)^2)}$.

In this section, given a matrix group $G\leq \GL(n, q)$, we view $G$ as a 
permutation group on the domain $\F_q^n$, so basic tasks like membership testing 
and pointwise transporter can be solved in time $q^{O(n)}$ by permutation group 
algorithms. Furthermore a generating set 
of $G$ of size $q^{O(n)}$ can also be obtained in time $q^{O(n)}$. These 
algorithms are classical and can be found in \cite{Luks90,seress2003permutation}.

As mentioned in Section~\ref{sec:intro}, for \GrI, Luks' dynamic programming 
technique \cite{Luks99} can improve the brute-force  
$n!\cdot \poly(n)$ time bound to the $2^{O(n)}$ time bound, which can 
be understood as replacing the number of permutations $n!$ 
with the number of subsets $2^n$. 

In our view, Luks' dynamic programming technique 
is most transparent when working with the subset transporter problem. Given a 
permutation group $P\leq S_n$ and $S, T\subseteq [n]$ of size $k$, this technique 
gives a $2^k\cdot \poly(n)$-time algorithm to compute $P_{S\to 
T}:=\{\sigma\in P : \sigma(S)=T\}$ \cite{BQ12}. To illustrate the idea in the 
matrix group 
setting, we 
start with the subspace transporter problem.
\begin{problem}[Subspace transporter problem]
Let $G\leq \GL(n, q)$ be given by a set of generators, and let $V$, $W$ be two subspaces 
of $\F_q^n$ of dimension $k$. The subspace transporter problem asks to compute the 
coset $G_{V\to W}=\{g\in G : g(V)=W\}$.
\end{problem}
The subspace transporter problem admits the following brute-force algorithm. Fix a 
basis $(v_1, \dots, v_k)$ of $V$, and enumerate all ordered basis
%\yinan{used to be ``bases''} 
of $W$ at the 
multiplicative cost of $q^{k^2}$. For each ordered basis $(w_1, \dots, w_k)$ of 
$W$, 
compute the coset $\{g\in G : \forall i\in[k], g(v_i)=w_i\}$ by using a sequence 
of 
pointwise stabiliser algorithms. This gives an 
algorithm running in time $q^{k^2+O(n)}$. 
Analogous to the permutation group setting, we aim to replace $O(q^{k^2})$, the 
number of ordered basis of 
$\F_q^k$, 
with $q^{\frac{1}{4}k^2+O(k)}$, the number of subspaces in $\F_q^k$, via a dynamic 
programming technique. For this we first observe the following. 

\begin{observation}\label{obs:enumerate_subspace}
There exists a deterministic algorithm that enumerates all subspaces of $\F_q^n$, 
and for each subspace computes an ordered basis, in time $q^{\frac{1}{4}n^2+O(n)}$.
\end{observation}
\begin{proof}
For $d\in \{0, 1, \dots, n\}$, let $S_d$ be the number of dimension-$d$ subspaces 
of $\F_q^n$. The total number of subspaces in $\F_q^n$ is 
$S_0+S_1+\dots+S_n=q^{\frac{1}{4}n^2+O(n)}$. To 
enumerate 
all subspaces we proceed by induction on the dimension in an increasing order. The 
case $d=0$ is trivial. For 
$d\geq 1$, suppose all subspaces of dimension $d-1$, each with an ordered basis, 
are listed. To list all subspaces of dimension $d$, for each dimension-$(d-1)$ 
subspace $U'$ with an ordered basis $(u_1, \dots, u_{d-1})$, for each vector 
$u_d\not\in U'$, form $U$ with the ordered basis $(u_1, \dots, u_d)$. Then test 
whether $U$ has been listed. If so discard it, and if not add $U$ together with 
this ordered basis to the list. The two for loops as above adds a multiplicative 
factor of at most $S_{d-1}\cdot q^{n}$, and other steps are basic linear 
algebra tasks. Therefore the total complexity is $\sum_{i=0}^nS_i\cdot 
q^{O(n)}=q^{\frac{1}{4}n^2+O(n)}$. %$\yinan{used to be $\sum_{i=0}^d$}. 
\end{proof}

%In the \GrI setting, dynamic programming technique  Now in subspace transporter 
%problem, analogously 
%we enumerate the number of subspaces of $W$ instead of enumerating all bases. 
%The number of subspaces of a $k$-dimensional space over $\F_q$ is upper bounded 
%by 
%$q^{\frac{1}{4}k^2+O(k)}$, which is slightly better than that of 
%enumerating bases. %In fact, we have the following.
\begin{theorem}\label{thm:subsp_trans}
There exists a deterministic algorithm that solves the subspace transporter 
problem in time $q^{\frac{1}{4}k^2+O(n)}$. 
\end{theorem}
\begin{proof}
We fix an ordered basis $(v_1, \dots, v_k)$ of $V$, and for $d\in[k]$, let 
$V_d=\langle v_1, \dots, v_d\rangle$. 
The dynamic programming table is a list, indexed by 
subspaces $U\leq W$. For $U\leq W$ of dimension $d\in[k]$, the corresponding cell 
will store the coset
$G(V_d\to U)=\{g\in G:g(V_d)=U\}$. When $d=k$ the corresponding cell gives $G(V\to 
W)$. 

We fill in the dynamic programming table according to $d$ in an increasing order. 
For $d=0$ the problem is trivial. 
%
%$d=1$, this is equivalent to compute $G(V_1\to \langle u\rangle)=\{g\in 
%G:g(v_1)=u\}$ for all $u\in W$. If we embed $G$ into a subgroup of the symmetric 
%group $\sym(\Omega)$ where $|\Omega|=q^n$, $G(v_1\to u)$ is just the coset of 
%point-wise stabilizer. We can apply the classical pointwise transporter algorithm 
%to compute 
%$G(v_1\to u)$ in time $q^{O(n)}$.
Now assume that for some $d\geq 1$, we have computed $G(V_l\to U')$ for all $0\leq 
l\leq 
d-1$ and subspace $U'\leq W$ of dimension $U$. To compute $G(V_d\to U)$ for some 
fixed $U\leq W$ of dimension $d$, note that any $g\in G(V_d\to U)$ has to map 
$V_{d-1}$ 
to some $(d-1)$-dimension subspace $U'\leq U$, and $v_d$ to 
some vector $u\in U\setminus U_0$. This shows that
$$G(V_d\to U)=\bigcup_{U'\leq U, \dim(U')=d-1}\bigcup_{u\in U\setminus 
U'}[G(V_{d-1}\to U')](v_d\to u).$$
To compute $[G(V_{d-1}\to U')](v_d\to u)$, we read $G(V_{d-1}\to U')$ from the 
table, then compute $[G(V_{d-1}\to U')](v_d\to u)$ using the pointwise transporter 
algorithm. The number of $u$ in $U\setminus U'$ is no more than $q^d$, and the 
number of $(d-1)$-dimension subspaces of $U$ is also no more than $q^d$. After 
taking these two unions, apply Sims' method to get a generating set of size 
$q^{O(n)}$. Therefore for each cell the time complexity is $q^{2d}\cdot 
q^{O(n)}=q^{O(n)}$. Therefore the whole dynamic programming table can be filled in 
time $q^{\frac{1}{4}k^2+O(k)}\cdot q^{O(n)}=q^{\frac{1}{4}k^2+O(n)}$.
%
%Denote $G(U_0,w)=\{g\in G(V_{k-1}\to 
%U_0):g(v_k)=u\}$. We are able to read out $G(V_{k-1}\to U_0)$ from the dynamic 
%programming table, and compute $g(v_k,u)$ in time $q^{O(n)}$ by the same 
%techniques. Thus, we can compute $G(U_0,w)$ in time $q^{O(n)}$. 
%The number of $(d-1)$-dimension subspaces of $U$ is $\gbinom{d}{d-1}{q}=O(q^d)$, 
%and the number of $w$'s is $q^d-q^{d-1}=q^{O(d)}$. In addition, we use Sims' 
%method (see details and algorithms in \cite{seress2003permutation,Luks90}) to 
%obtain a generating set of $G(V_d\to U)$ of size $q^{O(n)}$ in time $q^{O(n)}$. 
%Thus computing $G(V_d\to U)$ can be done in time $q^{O(n)}$ (since $q^{O(d)}$ can 
%be upper bounded by $q^{O(n)}$). 
%Finally, notice that the number of subspaces of $W$ is 
%\begin{equation}\label{enumerate bound 1}
%\sum_{j=1}^k\gbinom{k}{j}{q}\leq k\gbinom{k}{\lceil 
%\frac{k}{2}\rceil}{q}=n\cdot\frac{(1-q^k)\cdots(1-q^{k-\lceil 
%\frac{k}{2}\rceil+1})}{(1-q^{\lceil \frac{k}{2}\rceil})\cdots(1-q)}.
%\end{equation}
%Since $q^{k-j}\leq \frac{1-q^k}{1-q^j}\leq q^{k-j+1}$ for any $1\leq j\leq k$, we 
%can derive that
%\begin{equation}\label{enumerate bound 2}
%q^{\frac{1}{4}k^2}\leq\frac{(1-q^k)\cdots(1-q^{k-\lceil 
%\frac{k}{2}\rceil+1})}{(1-q^{\lceil \frac{k}{2}\rceil})\cdots(1-q)}\leq 
%q^{\frac{1}{4}k^2+\frac{1}{2}k}.\end{equation}
%This suggests that the number of subspaces of $W$ is upper bounded by 
%$q^{\frac{1}{4}k^2+O(k)}$.
%
%In conclusion, we can compute $G(V\to W)$ in time $q^{\frac{1}{4}k^2}\cdot 
%q^{O(n)}$ deterministically.
\end{proof}

To apply the above idea to \AltMatSpIso, we will need to deal with 
the following problem. 

%The similar idea can be applied to obtain an algorithm to solve \AltMatSpIso. 
%Recall that if we are given $\cG,\cH\leq\Lambda(n,q)$ of dimension $m$, the 
%brute-force algorithm for \AltMatSpIso runs in time $q^{n^2}\cdot \poly(m)$, by 
%enumerating all elements in $\GL(n,q)$. And the best known algorithm for 
%\AltMatSpIso offers a running time of 
%$q^{\frac{1}{4}(n+m)^2+O(1)}$ when $q=p$ is a prime \cite{Ros13a}. Before proving 
%how dynamic programming can be applied to obtain the time complexity in 
%theorem~\ref{thm:minor}, we consider Alt-matrix transporter problem.

\begin{problem}[Alternating matrix transporter problem]
Let $H\leq \GL(n, q)$ be given by a set of generators, and let $A, B\in \Lambda(n, 
q)$ be two alternating matrices. The alternating matrix transporter 
problem asks to compute the coset $H_{A \to B}=\{g\in H: \trans{g}Ag=B\}$. 
\end{problem}

\begin{theorem}\label{thm:alt_form}
There exists a deterministic algorithm that solves the alternating matrix 
transporter 
problem in time $q^{\frac{1}{4}n^2+O(n)}$. 
\end{theorem}

\begin{proof}
Let $(e_1,\dots,e_n)$ be the standard basis vectors of $\F_q^n$, and let 
$E_d=\langle e_1,\dots,e_d\rangle$. For an alternating matrix $B$, and an ordered 
basis $(u_1, \dots, u_d)$ of a dimension-$d$ $U\leq \F_q^n$, $B|_U$ denotes the 
$d\times d$ alternating matrix $[u_1, \dots, u_d]^tB[u_1, \dots, u_d]$, called the 
restriction of $B$ to $U$. For a vector $v$ and $U$ with the ordered basis as 
above, $B_{U\times v}=[u_1, \dots, u_d]^tB v\in \F_q^d$.

Then we construct a dynamic programming table, which is a list indexed by all 
subspaces of $\F_q^n$. Recall that each subspace also comes with an ordered basis 
by 
Observation~\ref{obs:enumerate_subspace}. For any $U\leq 
\F_q^n$ of dimension $k$, its corresponding cell will store the coset
\begin{equation}
H(A|_{E_k}\to B|_{U})=\{g\in H: g(E_k)=U,~\trans{g}(A|_{E_k})g=B|_{U}\}.
\end{equation}
%where $B|_{U}$ is the restriction of $B$ on subspace $U$.

We will also fill in this list in the increasing order of the dimension $d$. The 
base case $d=0$ is trivial. 
%When $d=1$, for any $U=\langle u\rangle$, $H(A|_{E_k}\to B|_{U})=\{g\in 
%H:g(e_1)=u\}$, since a $1\times 1$ alternating matrix is just $0$. In this case 
%$H(A|_{E_k}\to B|_{U})$ can be computed in time 
%$\poly(q^{n})$ by one application of the pointwise t
Now, assume we have already compute $H(A|_{E_l}\to B|_{U'})$ for all $0\leq l\leq 
d-1$ and subspace $U'\leq \F_q^n$ of dimension $l$. To compute $H(A|_{E_k}\to 
B|_{U})$ for some $U\leq \F_q^n$ of dimension $d$, note that any $g\in 
H(A|_{E_d}\to B|_{U})$ satisfies the following. Firstly, $g$ sends $E_{d-1}$ to 
some dimension-$(d-1)$ subspace $U'\leq 
U$, and $A|_{E_{d-1}}\in \Lambda(d-1, q)$ to $B|_{U'}\in \Lambda(d-1, q)$. 
Secondly, $g$ sends $e_d$ to some vector 
$u\in U\setminus U'$, and $A|_{E_{d-1}\times e_d}\in \F_q^{d-1}$ to $B|_{U'\times 
u}\in \F_q^{d-1}$. 
This shows that 
\begin{equation}
H(A|_{E_d}\to B|_{U})=\bigcup_{U'\leq U,\dim(U')=d-1}\bigcup_{u\in U\setminus 
U'}\big[[H(A|_{E_{d-1}}\to B|_{U'})](e_d\to u)\big](A|_{E_{d-1}\times e_d}\to 
B|_{U'\times 
u}).
\end{equation}
To compute $\big[[H(A|_{E_{d-1}}\to B|_{U'})](e_d\to u)\big](A|_{E_{d-1}\times 
e_d}\to 
B|_{U'\times 
u})$, we read $H(A|_{E_{d-1}}\to B|_{U'})$ from the table, compute 
$[H(A|_{E_{d-1}}\to B|_{U'})](e_d\to u)$ using the pointwise transporter 
algorithm. As $[H(A|_{E_{d-1}}\to B|_{U'})](e_d\to u)$ induces an action 
on $\F_q^{d-1}$ corresponding to the last column of $A|_{E_d}$ with the last entry 
(which is $0$) removed, 
$\big[[H(A|_{E_{d-1}}\to B|_{U'})](e_d\to u)\big](A|_{E_{d-1}\times e_d}\to 
B|_{U'\times u})$ can be computed by another pointwise transporter algorithm. 
As in Theorem~\ref{thm:subsp_trans}, we go over the two unions and apply Sims' 
method to obtain a generating set of size $q^{O(n)}$. The time complexity for 
filling in each cell is seen to be $q^{2d}\cdot q^{O(n)}$, and the total time 
complexity is then $q^{\frac{1}{4}n^2+O(n)}$.
%
%we fix a subspace $U'\leq 
%U$ of dimension $k-1$ and a vector $u\in U\setminus U'$, and compute
%\begin{equation}
%H(U',w)=\{h\in H(A|_{E_{k-1}}\to B|_{U'}): h(e_k)=w,~h(A|_{e_k\times 
%E_{k-1}})=B|_{w\times U'}\},
%\end{equation}
%where $A|_{e_k\times E_{k-1}}$ and $B|_{w\times U'}$ are column vectors of 
%dimension $k-1$, whose $j$th entries are $e_k^tAe_j$ and $w^tBu_j$ 
%($\{u_1,\dots,u_{k-1}\}$ forms an orthonormal basis of $U'$, which is fixed when 
%fixing $U'$), respectively. $H(A|_{E_{k-1}} \to B|_{U'})$ can be read from the 
%dynamic programming table, and computing $\{h\in H(A|_{E_{k-1}}\to B|_{U'}): 
%h(e_k)=w\}$ and $\{h\in H(A|_{E_{k-1}}\to B|_{U'}): h(A|_{e_k\times 
%E_{k-1}})=B|_{w\times U'}\}$ can be done in time $q^{O(n)}$ by the same 
%techniques 
%in theorem \ref{thm:subsp_trans}. Notice that
%\begin{equation}
%H(A|_{E_k}\to B|_{U})=\bigcup_{U'\leq U,\dim(U')=k-1}\bigcup_{w\in U\setminus 
%U'}H(U',w),
%\end{equation}
%and the number of $U'$ and $w$ are $\gbinom{k}{k-1}{q}$ and $q^k-q^{k-1}$, 
%respectively. We can compute $H(A|_{E_k}\to B|_{U})$ in time $\poly(q^{n})$. In 
%addition, we use Sims' method to obtain a generating set of $H(A|_{E_k}\to 
%B|_{U})$ of size $q^{O(n)}$ in time $q^{O(n)}$. Since the number of subspaces of 
%$\F_q^n$ is upper bounded by $q^{\frac{1}{4}(n^2)+O(n)}$, we can solve the 
%alt-matrix transporter problem in time $q^{\frac{1}{4}n^2+O(n)}$ 
%deterministically. 
\end{proof}

We are now ready to prove Theorem~\ref{thm:minor}.

\paragraph{Theorem~\ref{thm:minor}, restated.}
Given $\bG=(G_1, \dots, G_m)$ and $\bH=(H_1, \dots, H_m)$ in $\Lambda(n, q)^m$ 
representing $m$-alternating spaces 
$\cG, \cH\leq \Lambda(n, 
q)$, there exists a deterministic algorithm for \AltMatSpIso in time 
$q^{\frac{1}{4}(m^2+n^2)+O(m+n)}$.
%\medskip
\begin{proof}
Let $(e_1,\dots,e_m)$ be the standard basis of $\F_q^m$, and let $E_k=\langle 
 e_1,\dots, e_k\rangle$. 
$v=(a_1, \dots, a_m)^{\mathrm{t}}\in \F_q^m$, define 
$\bH^v:=\sum_{i\in[m]}a_iH_i\in \Lambda(n, q)$. 
For a dimension-$k$ subspace $V\leq \F_q^m$ with an ordered basis $(v_1, \dots, 
v_k)$, $\bH^V:=(\bH^{v_1}, \dots, \bH^{v_k})\in \Lambda(n, q)^k$. 
%Let $\cH^V$ be the dimension-$k$ subspace spanned by matrices in $\bH^V$.

The dynamic programming table is indexed by subspaces of 
$\F_q^m$, so the number of cells is no more than $q^{\frac{1}{4}m^2+O(m)}$. The 
cell corresponding to a dimension-$k$ subspace $V$ stores the coset
\begin{equation}
\Iso(\bG^{E_k}, \bH^{V})=\{(g, h)\in \GL(n,q)\times \GL(k, q): 
\trans{g}(\bG^{E_k})g=(\bH^V)^h\},
\end{equation}
%where $\cH|_V$ is obtained by considering $\cH$ as a bilinear map from $U\times 
%U\to W$, where $U\cong\F_q^n$
%and $W\cong\F_q^m$. Then $\cH|_V:U\times U\to V$ is the bilinear map obtained by 
%restricting the image of $\cH$ to $V$. 
%Notice that for any $1$-dimensional subspace $V=\langle v\rangle$, 
%$GL(\cG|_{E_1}\to\cH|_{V})$ can be computed efficiently, since it is the 
%isomorphism 
%between two alternating matrices. 
%\yinan{what is the exact time complexity?}

We will fill in the dynamic programming table in the increasing order of the 
dimension $d$. Recall that each subspace also comes with an ordered basis 
by 
Observation~\ref{obs:enumerate_subspace}. The base case $d=0$ is trivial. 
Now assume we have computed $\Iso(\bG^{E_{\ell}}, \bH^V)$ for all $1\leq \ell\leq 
d-1$ and $V\leq \F_q^n$ of dimension $\ell$. To compute $\Iso(\bG^{E_d}, \bH^V)$ 
for $V\leq \F_q^n$ of dimension $d$, note that any $h$ in $(g, h)\in 
\Iso(\bG^{E_d}, \bH^V)$ satisfies the following. Firstly, $h$ sends $E_{d-1}$ to 
some dimension-$(d-1)$ subspace $V'\leq V$, and $(g, h)\in \Iso(\bG^{E_{d-1}}, 
\bH^{V'})$. Secondly, $h$ sends $e_k$ to some $v\in V\setminus V'$, and $g$ sends 
$\bG^{e_d}$ to $\bH^{v}$. This shows that 
$$
\Iso(\bG^{E_d}, \bH^V)=\bigcup_{V'\leq V,\dim(V')=d-1}\bigcup_{v\in V\setminus V'} 
\big[[\Iso(\bG^{E_{d-1}}, \bH^{V'})](e_d\to v)\big](\bG^{e_d} \to \bH^{v}).
$$
To compute $\big[[\Iso(\bG^{E_{d-1}}, \bH^{V'})](e_d\to v)\big](\bG^{e_d} \to 
\bH^{v})$, $\Iso(\bG^{E_{d-1}}, \bH^{V'})$ can be read from the table.
$[\Iso(\bG^{E_{d-1}}, \bH^{V'})](e_d\to v)$ is an instance of the pointwise 
transporter problem of $\GL(n, q)\times \GL(k, q)$ acting on $\F_q^m$, which can 
be solved in time $q^{O(m)}$. Finally 
$\big[[\Iso(\bG^{E_{d-1}}, \bH^{V'})](e_d\to v)\big](\bG^{e_d} \to 
\bH^{v})$ is an instance of the alternating matrix transporter problem, which can 
be solved, by Theorem~\ref{thm:alt_form}, in time $q^{\frac{1}{4}n^2+O(n)}$. Going 
over the two unions adds a multiplicative factor of $q^{2d}$, and then we 
apply Sims' method to reduce the generating set size to $q^{O(n)}$. Therefore for 
each cell the time complexity is $q^{2d}\cdot 
q^{\frac{1}{4}n^2+O(n+m)}=q^{\frac{1}{4}n^2+O(m+n)}$. Therefore the whole dynamic 
programming table can be filled in in time $q^{\frac{1}{4}m^2+O(m)}\cdot 
q^{\frac{1}{4}n^2+O(n+m)}=q^{\frac{1}{4}(n^2+m^2)+O(n+m)}$.
%
%Now we assume that we have computed $GL(\cG|_{E_l}\to\cH|_{V'})$ for all $l\leq 
%k-1$ and subspace $V'\leq W$ of dimension $l$. We want to compute 
%$GL(\cG|_{E_k}\to\cH|_{V})=\{g\in \GL(n,q): g(\cG|_{E_k})g^t=\cH|_V\}$
%for some fixed subspace $V$ of dimension $k$. By the similar idea, we consider a 
%fixed subspace $V'\leq V$ of dimension $k-1$ and a fixed $w\in V\setminus V'$. To 
%compute the coset $GL(V',w)=\{g\in 
%GL(\cG|_{E_{k-1}}\to\cH|_{V'}):g(\cG|_{v_k})g^t=\cH_{w}\}$, we read the coset 
%$GL(\cG|_{E_{k-1}}\to\cH|_{V'})$ from the dynamic programming table and apply 
%theorem \ref{thm:alt_form} since $\cG|_{v_k}$ and $\cH_{w}$ is actually two 
%alt-matrices. Since
%\begin{equation}\label{transporter}
%GL(\cG|_{E_k}\to\cH|_{V})=\bigcup_{V'\leq V,\dim(V')=k-1}\bigcup_{w\in V\setminus 
%V'}GL(V',w).
%\end{equation}
%
%We know that computing $GL(\cG|_{E_k}\to\cH|_{V})$ can be done in time 
%$q^{\frac{1}{4}n^2+O(n)}$ for a $k$-dimension subspace $V$. In addition, we use 
%Sims' method to obtain a generating set of $GL(\cG|_{E_k}\to\cH|_{V})$ of size 
%$q^{O(n)}$ in time $q^{O(n)}$. In conclusion, we can solve $\AltMatSpIso$ in time 
%$q^{\frac{1}{4}(n^2+m^2)+O(n+m)}$ deterministically. 
\end{proof}