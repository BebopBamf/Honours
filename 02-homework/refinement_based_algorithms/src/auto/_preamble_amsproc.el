(TeX-add-style-hook
 "_preamble_amsproc"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "a4paper" "margin=2cm") ("inputenc" "utf8") ("fontenc" "T1") ("babel" "english") ("biblatex" "backend=bibtex" "style=alphabetic")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "proc-l"
    "proc-l10"
    "geometry"
    "hyperref"
    "inputenc"
    "fontenc"
    "babel"
    "biblatex"
    "mathtools"
    "amsmath"
    "amssymb")
   (LaTeX-add-bibliographies
    "references")
   (LaTeX-add-amsthm-newtheorems
    "theorem"
    "lemma"
    "corollary"
    "definition"
    "example"))
 :latex)

