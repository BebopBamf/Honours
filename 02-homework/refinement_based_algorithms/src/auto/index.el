(TeX-add-style-hook
 "index"
 (lambda ()
   (LaTeX-add-labels
    "thm:reduction-to-group"
    "alg:isometry-problem"
    "alg:refinement-procedure"
    "thm:refinement-bound"
    "alg:matrix-space-iso-testing"))
 :latex)

