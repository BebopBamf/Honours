(TeX-add-style-hook
 "proc-l"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "amsart"
    "amsart10")
   (TeX-add-symbols
    "ISSN"))
 :latex)

