\section{Introduction}

The \textit{Group Isomorphism Problem} asks to decide whether two
finite groups are isomorphic. The Group Isomorphism Problem belongs to
a particular class of problems unique in that they do not belong to
either P or are known to be NP-complete.

The \textit{Graph Isomorphism Problem} is the most widely studied and
famous problem within these classes of problems. Babai in 2015
proposed an algorithm that reduced the complexity of Graph Isomorphism
from exponential worst case to quasipolynomial worst case.

Currently, Babai's seminal algorithm stands as the
fastest deterministic worst-case algorithm for testing Graph
Isomorphism in time $\text{exp}((\log n)^{O(1)})$ with $n$ being the
number of vertices of a graph\cite{RN31}. However, it was noted
that the \textit{Group Isomorphism Problem} for finite groups was a
roadblock to placing Graph Isomorphism in P since Graph Isomorphism is
polynomial-time reducible to Group Isomorphism.

While significant progress has been made in reducing the complexity of
the Graph Isomorphism Problem, relatively little progress has been made
in reducing the complexity of the Group Isomorphism Problem. Miller, in
1978 showed that Group Isomorphism is testable in time
$n^{O(\log n)}$\cite{RN53}. In 2013, Rosenbaum made minor
improvements to the bounds, see~\cite{RN52}. Despite the considerable
research, no algorithm has been found showing group isomorphism runs in time
$n^{o(\log n)}$.

A notoriously difficult case of Group Isomorphism is testing
$p$-groups of (nilpotent) class 2 and exponent $p$. However a classic
result of Baer, often named Baer's correspondence shows that
$p$-groups of class 2 and exponent $p$ are polynomial time reducible
to the \textit{Skew-Symmetric Matrix Space Isometry
Problem}\cite{RN54}. This was a motivation for the study of linear
algebraic analogues of group isomorphism problems\cite{RN46, RN47,
RN36}. In 2023, Sun showed that $p$-group isomorphism could be tested
in time $n^{O(\log n)^{5/6}}$ in a major breakthrough, utilising
linear algebraic techniques and the canonical reduction to the
Skew-Symmetric Matrix Space Isometry Problem\cite{RN51}.

Refinement methods have been used historically to efficiently compute
isomorphism tests in practical cases, and to improve theoretical
bounds\cite{RN46, RN30, RN24, RN34, RN18}. The motivation to develop
refinement techniques for matrix spaces has been demonstrated by the
aforementioned papers in their use in reducing the complexity of the
previously mentioned problems. This paper presents a variation of a
refinement algorithm for matrix spaces, with techniques utilised in
Sun's algorithm.

\subsection{Preliminary Definitions and Notation}

In order to formally describe the results, here are the definitions
used within the paper.

In general the paper uses the following notational
conventions. $\mathbb{F}_p$ denotes a finite field of order $p$ where
$p > 2$ and $p$ is a prime number (so that using Baer's correspondance
$p$-group isomorphism is reducible to skew-symmetric matrix space
isometry over $\mathbb{F}_p$)\cite{RN54, RN51}.

A matrix space refers to a linear space composed of matrices.
$M(n \times m, \mathbb{F}_p)$ denotes the linear space of all $n
\times m$ matrices over $\mathbb{F}_p$, note that for $n$-square
matrices we can simply write $M(n, \mathbb{F}_p)$. Finally $GL(n,
\mathbb{F}_p)$ denotes the general linear group (the group of all
$n$-square invertible matrices over a finite field).

Recall that the $d$-dimension of a linear space denotes the size of
it's basis, and $\mathbf{A}^\top$ denotes the transpose of a matrix.

\begin{definition}[Matrix Rank]
  The rank of a matrix denotes the dimension of it's column space,
  (the space of columns of a given matrix). denoted
  $\text{rank}(\textbf{A})$.
\end{definition}

Here we formally define the notion of matrix space isometry.

\begin{definition}[Matrix Space Isometry]
  Let $\mathfrak{A}, \mathfrak{B} \leq_M M(n, \mathbb{F}_p)$ be the
  spanning sets of matrix spaces of $n \times n$ matrices of size. Two
  matrix spaces are said to be isometric if and only if there exists a
  $Q \in \text{GL}(n, \mathbb{F}_p)$ such that

  \[ 
    Q\mathfrak{A}Q^\intercal = \{ QAQ^\intercal \mid A \in
    \mathfrak{A} \} = \mathfrak{B}
  \]

\end{definition}

And the definition for matrix space equivalence.

\begin{definition}[Matrix Space Equivalence]
  Let $\mathfrak{A}, \mathfrak{B} \leq_M M(n, \mathbb{F}_p)$ be the
  spanning sets of matrix spaces of $n \times n$ matrices of size. Two
  matrix spaces are said to be equivalent if and only if there exists a
  $L, R \in \text{GL}(n, \mathbb{F}_p)$ such that

  \[ 
    L\mathfrak{A} = \{ LA \mid A \in
    \mathfrak{A} \} = \{ RB \mid B \in \mathfrak{B} \} = R\mathfrak{B}
  \]

  Note that this is equivalent to $L\mathfrak{A}R^{-1} = \mathfrak{B}$.

\end{definition}

Finally in order to formally analyse the results of the algorithm in
comparison to other algorithms, some additional definitions are
required.

\begin{definition}[Skew-Symmetric Matrices]
  Two matrices $\mathbf{A}, \mathbf{B}$ are said to be skew-symmetric
  if and only if $\mathbf{A} = -\mathbf{A}^\top$.
\end{definition}

Note that any linear combination of skew-symmetric matrices are
skew-symmetric.

\begin{definition}{Skew-Symmetric Matrix Space Isometry}
  Let $\mathfrak{A}, \mathfrak{B} \leq \mathbb{M}(n, \mathbb{F}_p)$ be
  skew-symmetric matrix spaces over finite fields of order $p$.

  $\mathfrak{A}$ and $\mathfrak{B}$ are said to be isometric if and
  only if there exists a $\textbf{T} \in \text{GL}(n, \mathbb{F}_p)$
  such that

  \[
    \mathbf{T}\mathfrak{A}\mathbf{T^\top} = \{
    \mathbf{T}\mathbf{A}\mathbf{T^\top} \mid \mathbf{A} \in
    \mathfrak{A} \} = \mathfrak{B}
  \]
  
\end{definition}

Finally we can write a formal relationship between the group
isomorphism problem and the skew-symmetric isomorphism problem.

\begin{theorem}[Reduction to $p$-group isomorphism of nilpotent class
  2 and exponent
  $p$~\cite{RN54,RN36,RN46}]\label{thm:reduction-to-group}
  
  There is a polynomial time reduction from the $p$-group of nilpotent
  class 2 and exponent $p$ and skew-symmetric matrix isometry problem
  such that solving group isomorphism in polynomial-time implies
  skew-symmetric isomorphism can be solved in time $q^{O(m + n)}$
  using Baer's correspondence.

  The same inverse reduction exists where $p > 2$.

\end{theorem}

\subsection{Main Result}

This paper presents an algorithm which computes matrix space isometry
of $\mathbb{M}(n, \mathbb{F}_p)$ matrix spaces in time $p^{O(k^2)} \cdot
\text{poly}(n,k,p)$, with matrices of high rank respect to a parameter $k$ and
$p > 2$ being prime.

The algorithm is a combination of a refinement technique introduced by
Sun~\cite{RN51}, and a brute force search algorithm introduced by Li and
Qiao~\cite{RN46}. It is notable that this produces a marginal improvement over a
brute force algorithm and only in cases where $k$ is less than $n$ and the
matrix space is of sufficiently high rank. However the technique can be combined
with other techniques in order to produce more significant results.

\section{The Matrix Space Isometry Algorithms}

Using the refinement technique for high rank matrices develop by Sun
we can define algorithmic techniques to test matrix space isometry
given we have suitably high rank matrices within our space.

Here we formally define the matrix space isometry decision problem.

\SetAlgorithmName{Problem}{problem}{List of all Problems}
\begin{algorithm*}
  \caption{The Matrix Space Isometry Problem for High Rank Matrices}
  \label{alg:isometry-problem}

  \KwIn{
    $\mathfrak{A}, \mathfrak{B} \leq_M M(m \times n, \mathbb{F}_p)$,
    be two matrix spaces with matrices of rank at least $4$.
  }

  \KwOut{\textbf{Yes}, or \textbf{No}}

  \ForEach{$T_l \in \text{GL}(n, \mathbb{F}_p), T_r \in \text{GL}(m,
    \mathbb{F}_p)$}{
    \If{\( \exists T_l \in \text{GL}(n, \mathbb{F}_p), T_r \in
      \text{GL}(m, \mathbb{F}_p), s.t., T_l \mathfrak{A} T_r = \{
      T_l A T_r \mid A \in \mathfrak{A} \} = \mathfrak{B}
      \)}{
      \KwRet Yes
    }

    \KwRet No
  }
\end{algorithm*}

\subsection{Matrix Space Refinement Techniques}

Given a matrix space $\mathfrak{A} \leq_M M({n}\times{m}, \mathbb{F}_p)$ we
can define two matrices $L \in M(\alpha \times m, \mathbb{F}_p)$ and
$R \in M(n \times{\beta}, \mathbb{F}_p)$. We define these matrices so
that we can ideally find distinguishable matrices $A \in \mathfrak{A},
LAR$, where $LAR$ is of size $\alpha \times \beta$ and $\alpha \leq m,
\beta \leq n$.

\subsection{The Brute Force Algorithm}

In order to figure out how first to approach matrix isometry it is
helpful to find some useful facts about a brute-force search.

\begin{lemma}[Time Complexity of Brute-Force]
  The brute-force algorithm runs in time $p^{n^2} \cdot
  \text{poly}(n,p)$ of matrix spaces size $n \times n$. 
\end{lemma}

\begin{proof}
  To check the number of linear transformations that could be applied
  to a matrix given over a finite field of order $p$, we could check
  each entry in the matrix.

  Thus checking each entry would yield a search of $p$ to the power of
  the entries of a matrix.
  
  There are $n^2$ entries in a matrix of a $n \times n$ matrix
  space. To check if there is a linear map from a given matrix to
  another matrix, it would thus require $p^{n^2}$ entries,
  if we have a polynomial amount of matrices that span a space, we
  have a time-bound of $p^{n^2} \cdot \text{poly}(n,p)$.
\end{proof}

\begin{corollary}
  By Theorem~\ref{thm:reduction-to-group}. We have a quasi-polynomial
  brute-force algorithm for p-group isomorphism if we apply the
  brute-force search to the skew-symmetric matrix isometry problem.  
\end{corollary}

While the brute-force algorithm yields a sub-optimal result, we have
some helpful information. Given that the brute-force algorithm depends
on the row-size of a $n \times n$ matrix space, it suggests a method
to improve the complexity.

\subsection{The Refinement Procedure}

Here we describe a method to decrease the size of matrices in a matrix
space with retaining enough information to yield a distinct basis for
a matrix space.

Given our aim is to reduce the size of matrices from some $n \times n$
to a given $k \times k$, we take the definition of lexically 
smaller matrices from Sun.

The refinement method utilises a technique to distinguish matrices.

Given a matrix space of size $n$ over a field of order $p$,
$\mathfrak{A}$, we can produce two matrices $\mathbf{L} \in
\mathbb{M}(k, n, \mathbb{F}_p)$ and $\mathbf{R} \in \mathbb{M}(n, k,
\mathbb{F}_p)$, such that a $n \times n$ matrix $\mathbf{A} \in
\mathfrak{A}$ has the property $\mathbf{L}\mathbf{A}\mathbf{R} =
\mathbf{B}$, where $\mathbf{B}$ is a $k \times k$ matrix.

Ideally we would like each matrix $\mathbf{L}\mathbf{A}\mathbf{R}$ to
be unique in the matrix space $\mathfrak{A}$. Here, we also provide
the helpful lemma from Sun.

\begin{lemma}[Rank Uniqueness~\cite{RN51}]
  Given a rank $r$, where $r \geq 4$, we can obtain a $k$ for
  $\mathbf{L}$ and $\mathbf{R}$, where $k$ is defined
  by a parameter $t$ described in~\cite{RN51}, we
  can obtain a unique matrix $\mathbf{L}\mathbf{A}\mathbf{R}$ where
  $\mathbf{A}$ is not the zero matrix and the rank of the matrix is
  at least $r$.
\end{lemma}

Sun provides a probabilistic proof in his paper.

Here we can now formally define a refinement procedure.

\SetAlgorithmName{Algorithm}{algorithm}{List of all Algorithm}

\begin{algorithm}
  \caption{Refinement Procedure}\label{alg:refinement-procedure}

  \KwIn{$\mathfrak{A} \leq_M M(n, \mathbb{F}_p)$, be a basis of a
    matrix space. Let $d$ be the dimension of $\mathfrak{A}$ and let
    the max rank be denoted $r$. Finally $\mathfrak{A}$ contains
    matrices of rank less than or equal to $4$.}

  \KwOut{$\mathfrak{B} \leq \mathbb{M}(k, \mathbb{F}_p)$, be a $k
    \times k$ matrix space}

  Let $\mathfrak{B} = \{ \}$ 

  Set $\mathbf{L} \in \mathbb{M}(k, n, \mathbb{F}_p)$ and $\mathbf{L}
  \neq 0$ 

  Set $\mathbf{R} \in \mathbb{M}(n, k, \mathbb{F}_p)$ and $\mathbf{L}
  \neq 0$ 

  \ForEach{$\mathbf{A} \in \mathfrak{A}$}{
    \If{$\mathbf{LAR}$ is not in the linear span of $\mathfrak{B}$ and
    is non-zero}{
      $\mathbf{B} = \mathbf{B} + \{ \mathbf{LAR} \}$
    }
  }

  Add the $\mathbf{0}$ matrix to the set $\mathfrak{B}$.

  \KwRet $\mathfrak{B}$
\end{algorithm}

\begin{lemma}
  \label{thm:refinement-bound}
  The algorithm runs in time $p^d \cdot \text{poly}(n, k, d, p)$
\end{lemma}

\begin{proof}
  Since the space $\mathfrak{A}$ is a basis it has $d$ elements by the
  definition of the dimension of a space. The algorithm enumerates $d$
  times with matrices over fields of order $p$. 
\end{proof}

\subsection{The Algorithm}

Here we can finally define an algorithm that searches for matrix space
isomorphisms.

\begin{algorithm}
  \caption{Refinement Procedure}
  \label{alg:matrix-space-iso-testing}

  \KwIn{$\mathfrak{A}, \mathfrak{B} \leq \mathbb{M}(n,
    \mathbb{F}_p)$, with rank $r, r \geq 4$}

  \KwOut{Yes or No}

  Let $\mathcal{A,B} =$ The matrices obtained by the refinement
  procedure with input $\mathfrak{A,B}$ respectively.

  \KwRet Brute force algorithm on $\mathcal{A,B}$. 
\end{algorithm}

\begin{lemma}
  The algorithm runs in time $p^{O(k^2)} \cdot \text{poly(n, k, p)}$.
\end{lemma}

\begin{proof}
  Step 1 runs in time given by Lemma~\ref{thm:refinement-bound}. Step
  2 runs in time $p^{k^2} \cdot \text{poly}(n,k,p)$. We can use big-O
  to hide the first step and since by the running time of brute-force
  being in respect to the amount of entries of given matrices in a
  matrix space, hence providing the resulting running bounds.
\end{proof}

\section{Conclusion}

\subsection{The Algorithm for Skew-Symmetric Matrix Spaces}

While the algorithm is suboptimal, when applied to skew-symmetric
space testing by Theorem~\ref{thm:reduction-to-group} it provides a
marginal improvement. However, for one the correctness of the
algorithm has not been verified and second more advanced methods using
the refinement technique have already been developed.

When compared to Suns algorithm for testing $p$-group isomorphisms, it
does not provide a worst-case guarantee so Sun's algorithm
contains a much better complexity\cite{RN51}. However, given that
Sun's algorithm is recent and a breakthrough in a difficult
problem. It provides an interesting foundation for exploration to see
at which points can an algorithm be improved in order to reduce his
algorithm even more.

\subsection{Advances in Isomorphism Testing}

This begs a restatement of the motivation of studying tensor
isomorphism, $p$-group isomorphism and other related isomorphism
problems in relation to the standard graph isomorphism.

While defining a quasipolynomial time algorithm for graph isomorphism
was difficult, it is notable that practically graph isomorphism has
been solved for a long time, see~\cite{RN34,RN18}. However, finding a
quasipolynomial time algorithm for Graph Isomorphism seemed to yield
no improvement to the time complexity of Group
Isomorphism\cite{RN46,RN31}.

Interestingly, hard cases of Group Isomorphism were known to be
trivially quasipolynomial time worst case. Except that very little
progress increasing the bound even from $n^{O(\log n)}$ to $n^{o(\log
  n)}$.

This algorithm demonstrates new techniques based on new linear
algebraic ideas that could be developed to make new progress in the
group isomorphism problem, a well known road-block to placing graph
isomorphism in P.

