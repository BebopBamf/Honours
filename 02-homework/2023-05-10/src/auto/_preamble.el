(TeX-add-style-hook
 "_preamble"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8")))
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "hyperref"
    "inputenc"
    "mathtools"
    "amssymb"))
 :latex)

