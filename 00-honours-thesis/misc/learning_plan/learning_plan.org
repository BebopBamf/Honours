#+title: The Complexity of Tensor Isomorphism and Applications to Quantum Information Learning Plan
#+author: Euan Mendoza
#+bibliography: isomorphism-testing.bib

* Overview

The honours project investigates /efficient/ tools and techniques used to analyse
and develop /faster algorithms for isomorphism testing/ specifically around the
complexity in the TI complexity class.

/Isomorphism Testing Problems/ is a rich topic that has fascinated researchers
from the inception of computational complexity. In computational complexity we
study the theoretical speed limits of algorithms and utilise rigorous models to
analyse the efficiency of algorithms [cite:@Karp1972].

By studying the mathematical structure known as a tensor, several new
breakthroughs have been made in introducing tools and techniques to analyse and
solve isomorphism testing problems the techniques of which are outlined in the
series /On the Complexity of Isomorphism Problems/
[cite:@GrochowQiao2023a;@GrochowQiao2021;@ChenEtAl2024;@GrochowQiao2023].

The first year of the project has focused on the computational complexity
aspect of tensor isomorphism testing, specifically looking at the groundbreaking
new techniques introduced by [cite:@Sun2023]. In which learning linear algebra
and abstract algebra was very important to this research. As well as learning
about computational complexity from the theory of computing science.

One area I found that was extremely prevalent in our research area and of which was
admittedly unexpected, was the field of  probability theory. I have identified
that it would be /very helpful/ to have some formal introduction to probability
theory, including probability distributions, advanced calculus/functional
analysis.

The focus of the research project in terms of practical applications is in the
field of /Quantum Information/. Here, we focus on tensor isomorphism testing in
the setting of complex fields such as the case in quantum mechanics. In doing so
we can re-frame the problem as determining the equivalence of 2 multipartite
quantum states.

While my foundational computer science skills I consider to be very solid, my
knowledge of the field of quantum information leaves much to be desired. While I
do not think that the project requires a large quantity of knowledge in the
domain of quantum information, it is very important for me to get a foundational
knowledge of the landscape of quantum information.

Finally there is also additional knowledge required such as diagramming skills
and computational algebra skills. So getting acquainted with the landscape of
computational algebra systems and graphing tools is very important, with a
specific preference towards LaTeX/TikZ and Magma.

* Learning Plan

The primary gap in knowledge is that of representation theory and quantum
information. I need to develop a very good conceptual understanding of the
concepts described such that I can appropriately understand the literature.
Therefore it is most important that I spend a bit of time working on learning
about Quantum Information. In order to do this I have enrolled in a EdX course
on quantum information and will do weekly exercises and notes on this course
estimating 4 to 6 hours a week until early April. This short course will
obviously not be enough to learn the scope of the problem, so I will also
supplement this with a directed studies course with my supervisor.

For the quantum information study, I will consider the EdX course completed by
watching the lectures (taking notes), and doing the problem sets. I will
consider having the requisite knowledge by having the ability to explain what a
pure and mixed multipartite quantum state is, additionally to explain what
equivalence under LU (Local Unitary), LOCC (Local Operations and Classical
Communications) and SLOCC (Stochastic Local Operations and Classical
Communications) means within the context of multipartite quantum states.

The dissertation might also require me to make and test an implementation of any
algorithm on a computer algebra system, specifically MAGMA. MAGMA operates very
similarly to MatLab, Mathematica, or SageMATH/Python. Getting familiar in
programming in a computer algebra system seems like a very useful transferable
skill. Attending the lectures for the beginner and advanced MatLab courses will
help me develop familiarity with these environments.

Additionally I also hope to learn some techniques for graphing cubes in MatLab
for graphical representation of the tensor mathematical object, and group
actions on them as we discuss in meetings with my supervisor.

A big part of our research involves meeting once or twice a week and giving
presentations on mathematical proofs or techniques. Having a sound conceptual
literacy of the topics discussed is an absolute requirement. To supplement this
I hope to take a formal course in probability theory so when we discuss concepts
such as the difference between an independent and dependant event in statistics
I have a much better conceptual understanding. In order to do this I have to
take a formal course in probability from EdX, here I don't need to know the full
depth of the field but it would be very helpful to learn about probability
density functions, and Bayesian statistics.

Additionally, it is paramount that I maintain a certain ability to calculate the
roots of quadratic equations, or understand the global maximum of functions when
understanding the bounds of probability functions. Here it would be very
beneficial to take a course in calculus to maintain these skills even if the
maths is not the kind we will be writing about.

In terms of learning outside of the required credit hours that is necessary to
complete the thesis. There is no harder exercise or maths than repeating the
results found within papers on algorithms. There are many famous proofs that I
need to understand in and out so I will be continuously be writing notes on
papers, and attempting to replicate the proofs and techniques utilised within
them to achieve the results. In doing so I will be both working toward my thesis
question, however and perhaps more importantly I will also be up skilling with a
body of work that I can look back on and reference.

In replicating others results, I most likely also will be required to give
presentations on them or give alternative proofs or improvements to them which
will be demonstrated to my supervisor for his critique.

* Summary

| Name                                                     | Platform | Hours |
|----------------------------------------------------------+----------+-------|
| Compulsory Module                                        | FEIT     |    10 |
| Introduction to Matlab                                   | FEIT     |    10 |
| Advanced Matlab                                          | FEIT     |    10 |
| DelftX: Fundamentals of Quantum Information              | EdX      |    20 |
| DelftX: Probability Theory                               | EdX      |    10 |
| Calculus                                                 | EdX      |    10 |
| Quantum Information/Representation Theory Directed Study | FEIT     |    10 |

Total hours: 80 hrs (8 cps)
Required: 6 cps

* Appendix
