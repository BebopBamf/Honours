#!/bin/sh

# Watch for changes in the current directory and run the given command
echo "Watching for changes in the current directory..."
watchexec -w . -e org,tex just
