\hypertarget{executive-summary}{%
\section{Executive Summary}\label{executive-summary}}

Algebraic Isomorphism Testing asks whether two objects in their
respective categories are structurally equivalent. Isomorphism Testing
is vital because we often treat structural equivalence as natural
equivalence instead of the mathematical definition, which does not
consider equality equivalent to isomorphism unless we take univalence as
our foundation of mathematics.

This inherent relation between equality and isomorphism is foundational
in the real world. In Computational Group Theory, this question may
interest those asking whether two molecules share the exact symmetry. In
quantum information and quantum cryptography, this is a fundamentally
important question.

Historically speaking, the question of finding an efficient algorithm
for isomorphism testing of \(p\)-groups of class 2 and exponent \(p\)
has been a widely studied problem as one of the most challenging cases
of group isomorphism. Xiaorui Sun showed isomorphism could be tested in
time \(N^{o(\log N)}\) where \(N\) denotes the group order, which is a
prime power \(p^{k}\). Additionally, there is a representation of
\(p\)-groups of class 2 and exponent \(p\) under Baer\textquotesingle s
correspondence to Alternating Matrix Spaces, as well as Alternating
Matrix Spaces being a fundamental TI-complete problem.

These relationships allow for a new project investigating potential
improvements synthesised based on these classical results. This project
proposes developing a new algorithm for testing \(p\)-groups of class 2
and exponent \(p\) and using this algorithm to improve the general
status quo of the TI-complete problems. At a high level, this is
achievable by improving the two new techniques developed by Sun in his
seminal paper. We additionally should hopefully be able to prove this
result holds for the class of even \(p\)-groups of class 2 exponent
\(p\) as well as being able to extend our results to the other
TI-complete problems in the same vein that Grochow and Qiao have
successfully managed to do in the recent past.

\newpage

\pagenumbering{arabic}

\hypertarget{introduction}{%
\section{Introduction}\label{introduction}}

This report proposes a new improvement to \emph{Algebraic and
Combinatorial Isomorphism Testing}. \emph{Isomorphism Testing} asks
whether two structures in their respective (mathematical) categories are
\emph{essentially} and \emph{structurally} equivalent. In practice, we
find that very few structures are mathematically equal. Working with
objects with the same \emph{structure} but not necessarily the same
\emph{composition of elements} is more straightforward in practice.

Abstractly dealing only with the structure of objects is far easier than
dealing with strictly equal objects. However, finding out if two
structures are equivalent is much more difficult. The
project\textquotesingle s primary goal is to uncover the algorithmic
complexity of the problem for the case of \(p\)-groups of (nilpotent)
class 2 and exponent \(p\). We also want to know how improvements can
propagate to related problems.

\emph{Historically, Graph Isomorphism} has presented itself as the most
widely studied isomorphism testing problem. Graph Isomorphism was one of
the first open questions in the development of computer science,
pointedly with the question asking whether Graph Isomorphism is P or
NP-complete \autocite{Karp1972}. However, while \emph{Graph Isomorphism}
is a rich field of research, the isomorphism testing of other algebraic
structures is a relatively prosperous and unexplored area presenting
exciting gaps in the current knowledge of computational complexity and
theoretical computer science.

The new development of an algorithm for testing \(p\)-groups of class 2
and exponent \(p\) in time \(N^{O((\log n)^{5/6})}\) by Xiaorui Sun
\autocite{Sun2023} presents an opportunity to improve the current best
known worst-case bounds of all the TI-complete isomorphism problems
\autocite{GrochowQiao2023}.

\hypertarget{isomorphism-testing-uses-in-industry}{%
\subsection{Isomorphism Testing Uses in
Industry}\label{isomorphism-testing-uses-in-industry}}

Isomorphism Testing has many valuable applications, specifically in
Post-Quantum Cryptography and Quantum Information. The leading group
primarily interested in the complexity of isomorphism testing is the UTS
QSI Centre of Quantum Science and Information under grant
\href{https://dataportal.arc.gov.au/NCGP/Web/Grant/Grant/LP220100332}{LP220100332}
and NIST ((American) National Institute of Standards and Technology).

In \emph{Post-Quantum Cryptography,} Isomorphism Testing is used as a
theoretical basis for \href{https://pqcalteq.github.io/}{ALTEQ,} which
is a current candidate for a post-quantum cryptography scheme based on
the trilinear forms equivalence problem, which is a TI-complete problem
\autocite{TangEtAl2022}.

Isomorphism Testing is also essential to Quantum Information, Machine
Learning and Computational Group Theory. The applications are described
in detail later on in the report.

\hypertarget{preliminary-definitions}{%
\subsection{Preliminary Definitions}\label{preliminary-definitions}}

The following notation and definitions are essential to the content of
the report. The following definitions can be found in the reference text
\autocite{Sipser2012}.

This report distinguishes the notion of an \emph{algorithm} from the
definition typically (mis)-used in literature. We can informally define
an algorithm as a set of instructions on a Turing-complete system that
always terminates on the \emph{correct} solution. When we move over to
the world of heuristics and practical algorithms, we give up on a proof
of correctness or a guarantee of termination. Heuristics are usually
distinguishable in literature since they utilise benchmarks and
real-world performance as a metric, where here, we analyse algorithms by
counting the maximum number of steps until they terminate. Here, we
distinctly distinguish algorithms that yield worst-case analysis as our
classical algorithms and practical and AI algorithms as our heuristics.

In \emph{computational complexity,} we classify algorithms based on
their respective worst-case running times in \emph{time complexity} and
worst-case space usage in \emph{space complexity.} Classically, we can
take P as the class of algorithms that terminate after a polynomial
number of steps on a deterministic Turing machine. We can also
understand NP as the class of algorithms that terminate in a polynomial
amount of steps in a non-deterministic Turing machine. The notion of
NP-complete is also vaguely useful in the isomorphism testing problem.
NP-complete defines the class of (decision) problems such that for a
given problem if it is in NP and every problem in NP is polynomial time
(Karp) reducible to that problem, it is said to be NP-complete.

A reduction from a problem \(A\) to a problem \(B\) means that we write
an algorithm that takes the inputs of \(A\) and modifies them to be the
inputs of \(B\). If \(A\) is reducible to \(B\), then we can use \(B\)
to solve \(A\) by changing the inputs to \(B\) and using an algorithm
that solves \(B\) to solve \(A\). In a sense, the principle of
reductions is why it is not a \emph{broad problem} to test all
TI-complete problems due to the property that if we solve a single
TI-complete problem, we can solve the rest in the complexity class by
using the algorithm obtained to solve the single instance.

For the case of this paper, we refer to Algebraic Isomorphism Testing as
the testing of TI-complete structures where TI-completeness refers to
the complexity class defined by Grochow and Qiao
\autocite{GrochowQiao2023}.

\hypertarget{overview-of-the-report-structure}{%
\subsection{Overview of the Report
Structure}\label{overview-of-the-report-structure}}

The report presents the goals of the proposed project first, introducing
the project aims and objectives. The report presents a brief yet
detailed overview of the history of the algebraic isomorphism testing
problem. Which we can use to describe, based on the \emph{status quo},
how significant the research is and what are the potential implications
of completing the project. We finally provide an overview of the
\emph{methodology} used to accomplish our objectives.

\hypertarget{research-aims-objectives}{%
\section{Research Aims \& Objectives}\label{research-aims-objectives}}

At a high level, this research aims to improve the current
state-of-the-art complexity of algebraic and combinatorial isomorphism
testing problems. Improving the current state of the art can be done by
accomplishing the following tasks.

In the project, a significant component is correctly
\emph{understanding} and \emph{improving} upon Sun\textquotesingle s
algorithm for \(p\)-group of class 2 exponent \(p\) \autocite{Sun2023}.
That is to develop a faster algorithm yielding worst-case analysis for
isomorphism testing of \(p\)-groups of class 2 and exponent \(p\).

In the case that we develop a new algorithm for \(p\) groups of class 2
exponent \(p\), we know that there is a reduction from \(p\)-groups of
class 2 exponent \(p\) to all other TI-complete isomorphism
problems\autocite{GrochowQiao2023,GrochowQiao2023a}. As such, it is
vital to know how the result potentially \emph{improves} and
\emph{extends} to the TI-complete problems, including 3-Tensor
Isomorphism, Alternating Matrix Space Isometry and the testing of
classes of Lie Algebras.

In his paper on \(p\)-groups of class 2 exponent \(p\), Sun introduces
two new techniques for isomorphism testing of (multi) linear structures,
specifically the techniques of \emph{high-rank matrix space
individualisation refinement} and \emph{low-rank matrix
characterisation}. Another project goal is to apply these techniques to
many other related problems. Notably, these techniques are related to
alternating matrix space isometry testing more closely than to
\(p\)-group of class 2 exponent \(p\) isomorphism testing. Similar
techniques may apply to matrix space isometry problems broader than
alternating matrix spaces.

Another significant objective of this project is to develop the
so-called linear algebraic analogues of the Weisfeiler-Leman technique.
In the research background section, the report describes how the
technique has been used to powerfully compute isomorphisms on
combinatorial structures. However, so far, linear algebraic models have
succeeded in the average case, but there are gaps in literature for the
development or contribution of analogues in the worst-case complexity.

\hypertarget{background}{%
\section{Background}\label{background}}

\hypertarget{graph-isomorphism-and-the-relationship-to-algebraic-isomorphism-testing}{%
\subsection{Graph Isomorphism and the Relationship to Algebraic
Isomorphism
Testing}\label{graph-isomorphism-and-the-relationship-to-algebraic-isomorphism-testing}}

In the initial investigation of the classic problem P vs NP, Karp asked
whether Graph Isomorphism was in P or NP-complete in 1972
\autocite{Karp1972}. Since then, there has been rapid development in the
isomorphism testing of Graphs. The impressive results in graph
isomorphism can be directly attributed to the success of the \emph{naive
colour refinement technique}
\autocite{BerkholzEtAl2017,CardonCrochemore1982}. The \(k\)-dimensional
Weisfeiler-Leman Algorithm generalised the results from the Colour
Refinement technique and has proved to be an incredibly powerful
foundation for Graph Isomorphism Testing
\autocite{WeisfeilerLeman1968,CaiEtAl1992}.

\begin{figure}[h]
\centering
\includegraphics[width=8cm]{colour-refinement.png}
\caption{Naive Colour Refinement}
\label{fig:colour-refinement}
\end{figure}

The Weisfeiler-Leman Algorithm is a powerful technique that appears in
some form in every practical and average case algorithm for Graph
Isomorphism. Most notably, graph isomorphism was shown to be linear
time-bounded in the average case \autocite{BabaiEtAl1980}. Graph
Isomorphism was also shown to be efficient in practice
\autocite{McKay1981,McKayPiperno2014}.

These results all gave strong evidence that Graph Isomorphism is likely
in the complexity class P instead of NP-complete, which are the hardest
problems in NP. It is known that Graph Isomorphism (and group
isomorphism) lie somewhere in the complexity class
\(\text{NP} \cap \text{co-AM}\). It is also a classic result that Graph
Isomorphism being NP-complete implies the collapse of the
polynomial-time hierarchy to the second level \autocite{Schoning1988}.

Using a different group-theoretic formulation of the Graph Isomorphism
Problem, Babai, in a landmark breakthrough, showed that Graph
Isomorphism was quasipolynomial time-bounded \autocite{Babai2016}. In
his seminal paper, Babai posed the question of \emph{how hard is Graph
Isomorphism}. In his paper, he referenced Group Isomorphism as a
potential roadblock to putting Graph Isomorphism in the Complexity Class
P. Babai also specifically noted that \(p\)-groups of class 2 and
exponent \(p\) seemed to be a particularly hard case of graph
isomorphism.

Grochow and Qiao also noted in the seminal paper on Tensor Isomorphism
completeness that Tensor Isomorphism is a roadblock to \(p\)-group
isomorphism of class 2 exponent \(p\) groups \autocite{GrochowQiao2023}.

\hypertarget{algebraic-isomorphism-in-connection-to-ti-complete-problems}{%
\subsection{Algebraic Isomorphism in Connection to TI-complete
problems}\label{algebraic-isomorphism-in-connection-to-ti-complete-problems}}

While Graph Isomorphism forms an interesting problem, it has effectively
hit a roadblock. Graph Isomorphism is seen as a problem that is
\emph{effectively} solved even though the question \emph{is Graph
Isomorphism in P} is still an open question. However, the algebraic
isomorphism testing of other algebraic structures are in their own
right, fascinating problems.

While Graph Isomorphism and the Isomorphism Testing of algebraic
structures are fundamentally intertwined, historically, they have
followed very different paths through literature. It should be noted
that Graph Isomorphism was studied for roughly 50 years before the
quasipolynomial time breakthrough. Even still, the quasipolynomial time
breakthrough was irrespective of the fact that very early on, Graph
Isomorphism had a practically efficient algorithm.

When compared to the history of Group Isomorphism, Group isomorphism was
known very early on to have a worst-case time bound of
\(N^{O( \log N )}\) where \(N\) denotes the group order, which was
initially attributed to Tarjan \autocite{Miller1978}. In 40 years, the
best-known algorithm for group isomorphism cannot improve this result
even to the marginal improvement of \(N^{o(\log N)}\)
\autocite{Rosenbaum2013}. In particular, an especially hard case of the
\emph{Isomorphism Problem for Groups} was the case of \(p\)-groups of
class 2 exponent \(p\).

The development of the complexity class TI and the notion of TI-complete
with a definition mirroring the NP-complete complexity class by Grochow
and Qiao provided a solid foundation to study this inherent difficulty
\autocite{GrochowQiao2023}. Importantly noting the shift from making
\emph{improvements} to understanding \emph{what makes Algebraic
Isomorphism Testing hard}. Notably, TI closely resembles work done by
Cook and Levin in the seminal result of proving SAT is NP-complete and
the corollary that 3-SAT is NP-complete. With the counterparts that
Tensor Isomorphism is TI-complete, and 3-Tensor Isomorphism is
TI-complete.

Isomorphism Testing can be viewed as a problem of given two elements of
a set \(X\) and a group acting on the set \(G\), decide if the two
elements are in the same \(G\)-orbit. Under this framework, we can view
Graph Isomorphism Testing as asking, given the set of graphs given by
their adjacency lists, are two graphs in the same orbit of the
permutation group acting on the graph. The seemingly much more difficult
instance of this problem is asking whether tensors given by three linear
basis and the general linear group acting on each basis are two tensors
in the same orbit.

\begin{figure}[h]
\centering
\includegraphics[width=8cm]{tensor-equivalence.png}
\caption{Pictoral View of Group Actions on a Tensor}
\label{fig:tensor-equivalence}
\end{figure}

Tensors can be constructed from Alternating Matrix Spaces, which are
linear representations of \(p\)-groups of class 2 exponent \(p\) given
by Baers Correspondence \autocite{Baer1938}. Some breakthroughs were
made in algebraic isomorphism testing using Baer\textquotesingle s
correspondence and linear representations. Notably, based on the success
of the Weisfeiler-Leman algorithm, efficient average case linear
analogues were developed\autocite{LiQiao2017,BrooksbankEtAl2019}.

Using ideas taken from utilising linear analogues of the
Weisfeiler-Leman algorithm, the first significant breakthrough in
\(p\)-groups class 2 exponent \(p\) testing was made by Xiaorui Sun by
introducing two new techniques for dealing with the aforementioned
Alternating Matrix Space Isometry Problem, namely \emph{high-rank
individualisation refinement} and \emph{low-rank matrix
characterisation} \autocite{Sun2023}. With low-rank matrix
characterisation being developed from the work of Flanders
\autocite{Flanders1962}. Using these new techniques, Grochow and Qiao
showed that his results extended to all the other TI-complete problems
\autocite{GrochowQiao2023a}.

\hypertarget{research-significance-innovation}{%
\section{Research Significance \&
Innovation}\label{research-significance-innovation}}

\hypertarget{significance-of-the-research}{%
\subsection{Significance of the
Research}\label{significance-of-the-research}}

As per the history of the algebraic isomorphism testing problem, there
are two avenues to explore when understanding the significance of
algebraic isomorphism testing.

From the perspective of Graph Isomorphism, algebraic isomorphism testing
is significant due to the properties exhibited by the Graph Isomorphism
Problem. Whether Graph Isomorphism is in P has been an open question
since the inception of Computer Science and Computational Complexity,
with the results showing that Graph Isomorphism is an easy problem in
both the practical case and the average case
\autocite{BabaiEtAl1980,McKay1981}, it is surprising that showing Graph
Isomorphism is in P has been such a difficult task. This is evidence
that there is a particularly hard case of Graph Isomorphism.

Since Graph Isomorphism is known to be \emph{easy} in practice, more
work has been done to understand what makes finding an efficient
algorithm in the worst-case for Graph Isomorphism so tricky. An
investigation into Graph Isomorphism with parameterised complexity has
been done to investigate which specific instances of Graph Isomorphism
are difficult. Specifically, Graph Isomorphism has parameterised
algorithms bounded by Treewidth, Graph Minors, Graph Euler Genus and
maximum degree \autocite{GroheNeuen2021a,GroheEtAl2023}. However, it is
essential to note the dichotomy of practically efficient algorithms
using combinatorial techniques to solve Graph Isomorphism, while the
best-known worst-case solutions utilise techniques from Group Theory.

It is believed that there must be a particularly hard case of Graph
Isomorphism, which forms a roadblock to placing Graph Isomorphism in P.
The connection between Graph Isomorphism and Group Theory, especially
within efficient worst-case methods of solving Graph Isomorphism has
long been thought to be a clue in this apparent difficulty. Here, Group
Isomorphism is classically reducible to Graph Isomorphism
\autocite{KoblerEtAl1993}. However, the significant lack of development
in the group isomorphism problem, especially the case of \(p\)-groups of
class 2 and exponent \(p\) is the predominant reason that experts
believe \textquotesingle Graph Isomorphism cannot be solved until we
solve Group Isomorphism\textquotesingle{} \autocite{Babai2016}.

As a side note, another perspective on why Algebraic Isomorphism
Problems are so attractive compared to the Graph Isomorphism Problem is
that Graph Isomorphism forms a zero-knowledge proof protocol
\autocite{GoldreichEtAl1991}. However, since Graph Isomorphism is easy
to calculate in practice, it is a fundamentally insecure protocol.
However, the algebraic isomorphism problems tend to be much harder to
calculate. As such, understanding the complexity of algebraic
isomorphism testing is essential to establish how effective a
cryptographic protocol based on algebraic isomorphism testing is
\autocite{TangEtAl2022}.

From the perspective of Algebraic Isomorphism Testing as a stand-alone
problem. (Nilpotent) \(p\)-groups of class 2 exponent \(p\) have long
stood as a hard case of the group isomorphism problem.
Sun\textquotesingle s algorithm is a significant breakthrough as it
opens a 40-year-long block in the development of algorithms for Group
Isomorphism \autocite{Sun2023}. We now have new techniques at our
disposal that have the potential to improve further isomorphism testing
algorithms for all of the known TI-complete algorithms. Work has been
done to show that Sun\textquotesingle s result extends to the other
TI-complete problems \autocite{GrochowQiao2023a}. However, there is room
for improvement in developing new and more efficient algorithms than the
last.

\hypertarget{potential-benefits-of-understanding-the-complexity-of-isomorphism-testing}{%
\subsection{Potential Benefits of Understanding the Complexity of
Isomorphism
Testing}\label{potential-benefits-of-understanding-the-complexity-of-isomorphism-testing}}

Isomorphism testing has numerous practical applications, all in need of
faster algorithms. Complexity gives a sense of what particular aspects
of a problem are hard and may allow further development in various
fields.

Understanding the Complexity of TI-complete problems is now more
critical than ever. It is becoming increasingly common knowledge that
Shor\textquotesingle s algorithm and Quantum Computing potentially
threaten information security \autocite{ASDACSC2023}. However, the
current NIST call for Post Quantum Cryptography Schemes all rely on
lattice-based cryptography standards. This means lattice-based schemes
could potentially suffer the same fate as factorisation-based
algorithms, which are weak to Shor\textquotesingle s algorithm.
Essentially, we need to diversify the landscape of cryptographic
protocols in order to ensure security. A cryptographic protocol based on
Alternating Trilinear Forms, a known TI-complete problem, was proposed
to accomplish this goal \autocite{TangEtAl2022}. Understanding the
difficulty within the TI-complexity class gives us clues as to whether
TI-complete problems can stand the test of time as a secure
Cryptographic Protocol.

In \emph{Quantum Information,} UTS QSI maintains an interest in this
area of research. Quantum Information is the area of study trying to
understand information-theoretic problems through the lens of
non-classical quantum physics. It is crucial to many industries, such as
finance, for its potential use in efficiently solving challenging
optimisation problems. However, this is still a field in the early
stages of development. Quantum Information, by being \emph{Quantum}
mathematically, is modelled as Tensor Products within Hilbert Spaces,
and a classic question asks if there is a relation of quantum states
under \emph{stochastic local operations and classical communications}
(SLOCC) \autocite{BennettEtAl2001}. However, this can be precisely
modelled as a problem of Tensor Isomorphism\autocite{DurEtAl2000}.

In \emph{Computational Group Theory,} the problem of TI-completeness and
\(p\)-group isomorphism for class 2 and exponent \(p\) groups is
fundamentally essential to developing more efficient algorithms within
Computational Algebra Software such as MAGMA and GAP. These tools are
used industry-wide by researchers in domains such as Chemistry, where
they ask if two chemical molecular structures are equivalent under
symmetry (equivalent to a group acting on a structure sharing the same
orbit).

In \emph{Machine Learning,} there are two domains in which isomorphism
problems are fundamentally important. In \emph{feature extraction,} we
can use tensor isomorphism to extract the \emph{signature tensor} from a
machine-learning model \autocite{PfefferEtAl2019,Chen1957}. Feature
extraction is used in \emph{cancer cell detection. As} such, cancer
researchers are primary stakeholders to feature extraction improvements.
Additionally, Graph Neural Networks proliferate in computer vision
through object detection and motion-tracking applications. The basis of
these networks has been shown to be equivalent to the famous
Weisfeiler-Leman technique described in the history section
\autocite{XuEtAl2019}.

\hypertarget{innovation-within-the-proposed-project}{%
\subsection{Innovation within the Proposed
Project}\label{innovation-within-the-proposed-project}}

In completing this research, we would have improved upon a 40-year-long
bottleneck in showing that Group Isomorphism Testing can be done in
\(N^{o( \log N )}\) time.

We hope to set the status quo based on Sun\textquotesingle s techniques.
We do not believe our current algorithms are the fastest they can be,
and given new techniques, we think we can improve these algorithms
further.

The innovation is not singly in creating faster and more efficient
algorithms. The most significant potential contribution to this project
would be showing that the 2-groups of class 2 and exponent \(p\) are
solvable in time \(N^{o(\log N)}\). The even groups are thought to
compose most \(p\)-groups of class 2 and exponent \(p\) despite \(p\)
representing a prime power. This is a big feat and would allow us to
show that all cases of \(p\)-groups of class 2 and exponent \(p\) are
little-o quasipolynomial time bounded.

Solving all \(p\)-groups is essential in understanding the complexity of
isomorphism testing problems. It has the potential to propagate through
every isomorphism problem and improve our current understanding of each
problem so that we may develop new techniques for optimising problems in
machine learning and quantum information. As well as understanding new
potential threats to cryptosystems based on alternating trilinear form
equivalence.

\hypertarget{research-methods}{%
\section{Research Methods}\label{research-methods}}

\hypertarget{research-methods-related-to-pure-mathematics}{%
\subsection{Research Methods Related to Pure
Mathematics}\label{research-methods-related-to-pure-mathematics}}

This research is within the space of the categorisation and complexity
analysis of intractable problems. Here, we only care about the worst
case.

In undertaking the research, we attempt to prove mathematically certain
properties of algorithms. As such, we borrow numerous methodologies from
the research of computational complexity.

In \emph{undertaking literature reviews,} we seek to understand what
makes techniques proposed in papers so powerful. This is important to
gain a \emph{conceptual} understanding of the problem and \emph{verify}
the accuracy of the information. Here, we will try to understand new
techniques proposed and find potential ways to build upon and improve
upon such techniques.

In \emph{developing new mathematical methods,} we seek to identify
\emph{mathematical} areas in which we can introduce new results. Each
\emph{new} theorem or \emph{lemma} must be verified with \emph{proof}
showing that the assumption is true based on \emph{formal logic}. An
expert, such as a fellow researcher in the same domain or a supervisor,
should verify each proof. Methods of presenting new proofs could be
presentations and written reports with accompanying proofs.

In \emph{optimising mathematical results,} we attempt to find flaws in
current known mathematical results and add improvements. The methodology
is the same as \emph{developing a new method} however, it is based on
prior knowledge where \emph{new methods} are original results.

In developing results, we will use mainly techniques taken from
\emph{linear} and \emph{abstract algebra,} which is the study of
\emph{linear} and \emph{abstract} algebraic structures. Here, we concern
ourselves with \emph{finite} structures. As such, we borrow knowledge
from \emph{representation theory,} a developed theory that asks how
abstract structures can be \emph{represented as linear structures}. Any
new knowledge that \emph{contributes} to the field of
\emph{representation theory} is considered progress. As such, we can
measure progress by describing new lemmas and theorems produced while
also accounting for their \emph{importance} within the current
literature to which they apply.

In developing results, we also heavily utilise the \emph{probabilistic
method,} which is a non-constructive proof technique that shows that
objects exist bound by specific structure given that there is a non-zero
constant probability of them existing within a set. In general,
\emph{probabilistic proofs} yield a range of potential improvements,
such as \emph{optimising the bounds} (which means we find a better
probability of something existing). This works well with the above group
and linear theoretic techniques and allows us to prove with certainty
that structures may exist.

\hypertarget{research-methods-to-algorithm-analysis-and-computational-complexity}{%
\subsection{Research Methods to Algorithm Analysis and Computational
Complexity}\label{research-methods-to-algorithm-analysis-and-computational-complexity}}

In \emph{Computational Complexity,} we utilise \emph{asymptotic
analysis} for the worst-case by asking the question, how many steps does
this algorithm take asymptotically on a deterministic Turing machine
with respect to a given input? Here, we try to find \emph{lower}
asymptotic bounds. We need to provide both a \emph{proof of
correctness,} which says that for every input string \emph{w,} the
Turing machine will \emph{halt} on the correct solution to the problem.
We also need a \emph{proof of time-bounds,} which is the analysis of how
many \emph{steps} it takes to compute the answer to the problem on a
\emph{deterministic single-tape Turing machine}. Our criteria for
success is if the algorithm has a \emph{lower asymptotic running time in
the worst case} than the last.

It is also important to show that specific problems are in a particular
\emph{complexity class}. As such, it is essential to use the
\emph{appropriate} reduction for the complexity class. For example, for
a reduction to a TI-complete problem, we must show that a given problem
is polynomial time Turing reducible to Tensor Isomorphism. Here, we have
specific \emph{criteria} in order to constitute a \emph{successful}
reduction. The reduction most often needs to be a \emph{computable
function,} which is a function that runs on a Turing Machine and halts
on the image of the function for every pre-image.

In most cases, the reduction will need to query an \emph{Oracle Turing
machine}, but the amount of queries defines the difference between Karp
and Turing reductions. We also say that for standard reductions, we want
\(\forall w \in L \Leftrightarrow f(w) \in L'\), or our function is
surjective. Other criteria usually depend on the \emph{type} of
reduction. Such as FPT reductions for the FPT complexity class or
polynomial time reductions to show an algorithm is in P or NP-complete.
A reduction is a critical research methodology as it \emph{classifies}
the difficulty of a problem. The introduction of new \emph{appropriate}
reductions verified by a \emph{proof of correctness} is considered good
progress in research in computational complexity.

\hypertarget{on-the-collection-of-data-and-evaluation-metrics}{%
\subsection{On the Collection of Data and Evaluation
Metrics}\label{on-the-collection-of-data-and-evaluation-metrics}}

It should be noted that usually, we cannot simply classify research in
Computational Complexity as \emph{qualitative} or \emph{quantitative}.
Qualitative implies the collection of experimental and statistical data.
In contrast, Qualitative describes the collection of data that is up to
interpretation and evaluated based on non-numerical and subjective
measures.

The results obtained within the project have the property that, once
verified, they \emph{should} not be up to interpretation. They should be
\emph{verified} formally. Of course, there is \emph{potential for
mistakes}. However, the results are \emph{not experimental}. In the same
vein, in contrast to qualitative data, we do not want our data to be
\emph{up to interpretation}. With this understanding, the data collected
from Computational Complexity research is an equal mix of qualitative
and quantitative metrics. In quantitative, we want to evaluate a result
based on a numerical answer without interpretation. Our qualitative
aspect is evaluating formal arguments in logic and making connections
between an abstract representation and broader concepts.

The \emph{type} of data collected will be a repository of lemmas and
corresponding \emph{proofs} utilising the techniques mentioned
previously. Each \emph{proof} can be verified as correct or incorrect
and may be compared based on select criteria. Such as does the proof
introduce new original knowledge to the current landscape. Does the
proof obtain tighter bounds than a previous result, or does the proof
generalise more objects than a previous result?

\emph{Specifically,} we hope to obtain a \emph{faster} algorithm in
\emph{deterministic worst-case} for the class of TI-complete problems,
which we can \emph{compare} with the current status quo.

\hypertarget{the-proposed-research-structure}{%
\subsection{The Proposed Research
Structure}\label{the-proposed-research-structure}}

In undertaking our research, we want a fixed research design to improve
\emph{the algorithm for testing isomorphism in p-groups of class 2
exponent p}. However, we may want a more flexible research design in
which we can \emph{explore} the \emph{implications} to other TI-complete
problems.

In doing so, we need to \emph{understand} the techniques proposed in the
algorithm by \emph{conducting literature reviews} and \emph{undertaking
presentation-style research activities}. We must also \emph{create a
body of Lemmas, Theorems and accompanying proofs}. Synthesising a faster
algorithm for \(p\)-groups of class 2 exponent \(p\) is feasible within
the project period.

However, developing new techniques based on the Weisfeiler-Leman
Algorithm, a significant component of Sun\textquotesingle s paper is a
broad and flexible research goal. After developing a faster algorithm
for the case of \(p\) groups of class 2 exponent \(p\), exploring the
implications, either through directly applying new techniques or through
a method of TI-complete reductions and analysis, is the more exciting
aspect of the project contained within the project proposal.

\hypertarget{conclusion}{%
\section{Conclusion}\label{conclusion}}

Algebraic Isomorphism Testing is arguably at the forefront of
mathematical and theoretical computer science. It has developed into one
of the most intriguing areas that puzzle mathematicians and computer
scientists alike.

Establishing faster deterministic worst-case algorithms for testing
TI-complete problems gives us new information on how to develop
algorithms in the practical sense. TI-complete algorithms have
classically been a challenging class of problems to solve.

This report describes the TI-complete problems as significant for
broadening the potential post-quantum cryptography schemes available to
us in a world dominated by lattice-based schemes. It also mentions the
importance of the result as a widely studied theoretical problem.

This report describes the very long and significant historical
connection and correlation between the isomorphism testing of
(multi)linear structures and the isomorphism testing of graphs. The
graph-based formulation of the problem hopes to utilise results obtained
in understanding the complexity of general algebraic isomorphism
described as group actions to improve results within its research
domain.

In understanding the overall depiction of isomorphism testing as a
problem and how it can be understood as a single open question in
research. The report proposes a new project in which we attempt to
improve the known worst-case time complexity status quo for isomorphism
testing. We describe the methodology and also describe how, when
understanding TI-complete problems as a homogeneous class of problems
given by polynomial-time Turing reductions, we can understand that any
faster result obtained for \(p\)-group of class 2 exponent \(p\)
isomorphism testing can be extended to the other isomorphism testing
problems. This is a result demonstrated in the following paper
\autocite{GrochowQiao2023}. As such, it is not unreasonable or
infeasible to think of this project\textquotesingle s scope as both
significant and reasonable within a year.
