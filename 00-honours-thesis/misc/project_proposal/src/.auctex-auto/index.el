(TeX-add-style-hook
 "index"
 (lambda ()
   (LaTeX-add-labels
    "executive-summary"
    "introduction"
    "research-aims-objectives"
    "background"
    "research-significance-innovation"
    "research-methods"))
 :latex)

