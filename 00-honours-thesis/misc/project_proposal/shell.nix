# { pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/40f79f003b6377bd2f4ed4027dde1f8f922995dd.tar.gz") {} }:

let
  pkgs = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/51d906d2341c9e866e48c2efcaac0f2d70bfd43e.tar.gz";
  }) {};
  
  tex_pkgs = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/79b3d4bcae8c7007c9fd51c279a8a67acfa73a2a.tar.gz";
  }) {};
in
  pkgs.mkShell {
    buildInputs = [
      tex_pkgs.biber
      pkgs.tectonic
      pkgs.pandoc
      pkgs.neovim
    ];
  }

