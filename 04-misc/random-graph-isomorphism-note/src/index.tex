\section{An Introduction}

Isomorphism Testing is a widely studied (by the 5 people in QSI who study it)
problem in theoretical computer science. The most famous of the isomorphism
problems is \textit{Graph Isomorphism} which has been investigated since the
initial exploration of the complexity classes P vs NP~\cite{Karp1972}.

An isomorphism is like a very loose equality, and isomorphism testing is a way
to ask if two things are \textit{loosely equal}. Mathematically this is
important because rarely are two things actually equal. In category theory, we
can say that two objects are isomorphic if their morphisms are invertible, in
category theory we might also treat this relationship with the same weight as
equality.

Graph Isomorphism exhibits a strange property that it lies somewhere in
$\text{NP} \cap \text{co-AM}$. We also know that if we prove that Graph Isomorphism
is in NP-complete, it implies the collapse of the polynomial time hierarchy to
the second level. As a result, experts deduce (just because it wasn't obvious)
that Graph Isomorphism is in NP-complete.

So the question remains, is Graph Isomorphism in P? Originally when I presented
random graph isomorphism I presented it as a thing that they did because they
could. However, I think the question of Graph Isomorphism in P is the real
reason they attempted to tackle this problem. At the time Graph Isomorphism was
still exponential time bounded, however it was not a straightforward P vs NP
problem. Consider Integer Factorisation, that historically has been hard for
single tape deterministic turing machines however that is probably
NP-intermediate.

Ladner's theorem states that either P = NP or there is a big space of problems
between P and NP-complete. Graph Isomorphism at first might seem likely to be a
candidate however, in practice it has all the criteria of being in P. It is easy
to compute on average and in practice~\cite{McKay1981,McKayPiperno2014}. Here I
present an average case algorithm for Graph Isomorphism. This result shows that
Graph Isomorphism is easy to solve over the majority of the inputs.

Another interesting motivation for this result is Cryptography. In theoretical
computer science we tend to focus on the worst-case bounds of an algorithm. In
cryptography we focus on what the average-case result is. The average-case tells
us whether a system is weak or strong in practice. Graph Isomorphism was
proposed as a potential RSA like cryptography system, have a think about why
this is likely ineffective?

\subsection{Preliminary Definitions}

Here are some of the basic definitions.

\begin{definition}
  Recall a graph is a pair $G = (\mathcal{V}, \mathcal{E})$ where

  \begin{enumerate}
    \item
          $\mathcal{V}$ is a finite set, called the \textit{vertex set}, and the function
          $V : G \to \mathcal{V}$, denoted as $V(G)$, returns the vertex set of a graph.
    \item
          $\mathcal{E} \subseteq \mathcal{V} \times \mathcal{V}$, is the \textit{edge set} of a graph. The function
          $E : G \to \mathcal{E}$, denoted as $E(G)$, returns the edge set of a graph. Also
          note that in a graph, the ordering of the pair does not matter (unless
          it does).
  \end{enumerate}
\end{definition}

The notion of a (homo)morphism and it's use in mathematics is incredibly
important. So much so that it essentially forms the basis for one of the
foundational fields of mathematics \textit{category theory}. How we describe
functions is incredibly important, a function can be called surject, injective
or bijective. It can also be called a homomorphism, an endomorphism or an
isomorphism. While the two are related they are not the same.

In order to understand why functions are so important, think of the proof that
the size of the odd numbers are the exact same as the size of the even numbers,
or even more mind blowing is that the size of the natural numbers (including or
excluding 0) is the exact same as the size of the integers and somehow is
\textit{not} the same as the size of the real numbers.

Before I describe functions it is also important to note that isomorphisms in
graphs are more surprising than isomorphisms between algebras. If you have a
group of rotational symmetry of a square and a integer group with 4 numbers they
are clearly different despite having a trivial isomorphism, but we don't talk
about two \textit{isomorphic graphs} like they are different, which is probably
why graph isomorphism (to me atleast) is atleast a little bit confusing.

\begin{definition}
  A function is defined so that for all elements in it's initial set (called
  domain) $D$ there is a corresponding element in it's codomain $C$. A function
  $f : D \to C$ is said to be \textit{injective} if and only if there are elements
  $x_{1}, x_{2} \in D$ such that $x_{1} \neq x_{2}$, than $f(x_{1}) \neq f(x_{2})$. That
  is that each element in $D$ maps to a distinct element in $C$. A function is
  said to be \textit{surjective} if and only if for all $y \in C$, there exists a
  $x \in D$ such that $f(x) = y$. This describes the property that all elements in
  $C$ have an arrow pointing to the element. If a function is injective, two
  arrows never point to the same element so clearly this implies $|D| \leq |C|$,
  and if a function is surjective every element in $C$ has a arrow so clearly
  $|D| \geq |C|$. Finally a function is said to be \textit{bijective} if and only
  if it is both surjective and injective. Clearly this also says that a function
  has a \textit{unique} arrow pointing toward each object in the respective
  sets. It also says $|C| = |D|$, and that we can obtain a inverse function
  $f^{-1} : C \to D$ by reversing the direction of the arrows (since they are
  uniquely mapped).
\end{definition}

Whether a function is bijective tells us some properties of the underlying set,
however they do not tell us about the underlying \textit{structure} of the
elements in the set. (I think) this is the leading motivation behind the idea of
a morphism. A homomorphism can be thought of just a function, in category theory
this is also called a morphism or a arrow, however the main difference is that a
function only cares about the underlying set while a morphism cares about
whether it is also strucure preserving.

It is hard to define homomorphisms for all objects, for groups, rings and fields
a homomorphism is just a function that maps a thing to another thing that is
also the same type of thing as the initial thing. For example a group
homomorphism just means a map between a group and another group, or a ring to
another ring. However, a morphism isn't necessarily always defined this way, a
morphism between two graphs doesn't necessarily mean that the start and end set
are graphs.

\begin{definition}
  A \textit{graph homomorphism} is a function $\phi : G \to H$ where $G$ and $H$ are
  graphs such that $\forall (v, u) \in E(G), \exists (\phi(v), \phi(u)) \in H$. Or all the edges in
  $G$ have a corresponding edge in $H$. A graph is \textit{isomorphic} or has an
  \textit{isomorphism} if and only if there exists a $\phi$ homomorphism and $\phi$ is
  bijective. An isomorphism between $G$ and $H$ is denoted $G \cong H$.
\end{definition}

Clearly a graph homomorphism is only for graphs with the same edge relation, and
similarly not all isomorphisms are necessarily bijective.

The corresponding decision problem is decide if there exists a function $\phi$
which makes $G \cong H$.

Sometimes we also like to generalise isomorphism even more, we want a framework
that is more abstract so that we can talk about isomorphism testing algorithms
in the context of Graph Isomorphism.

\begin{definition}
  A group $(G, \cdot)$ also sometimes written as simply $G$ is a set $G$ equipped
  with a binary function $\cdot : G \times G \to G$ which satisfies the following
  properties.

  \begin{enumerate}
    \item
          \textit{Associativity: } for all
          $x, y, z \in G, x \cdot (y \cdot z) = (x \cdot y) \cdot z$.

    \item
          \textit{Identity: } for every $x \in G$, there exists a $e \in G$ such
          that $x \cdot e = x = e \cdot x$.

    \item
          \textit{Invertability: } for every $x \in G$, there exists a $y \in G$
          such that $x \cdot y = e = y \cdot x$ where $e$ is the identity of the group.
          We can write $x^{-1}$ to mean the inverse element.
  \end{enumerate}

  If there is no confusion we can write $xy$ to mean $x \cdot y$. Finally if a group
  is also commutative, i.e. $\forall x, y \in G, xy = yx$, we say the group is
  \textit{abelian}.
\end{definition}

Usually we clasically take a group to be the integers since it satisfies those
properties. However in practice it is more useful to think of functions as
groups. Here I will introduce one of the most important groups of group theory,
the symmetric group also known as the permutation group (also known as the
sudoku puzzle).

\section{Part 1: An Introduction to Average Case Analysis}

Babai is a famous name in group theory and graph isomorphism. He is responsible
for the fastest graph isomorphism algorithm~\cite{Babai2016}. Erdos is probably
even more famous for his contribution to combinatorics and the probabilistic
method. I still don't know what Selkow was famous for but he has my respect.

In general we study average case to get a sense if problems are solvable in
practice or not. This is important for cryptography where it doesn't matter
whether a problem is P or NP-hard. It only matters if it doesn't break the
internet (RSA). However, I think Babai-Erdos-Selkow developed this analysis
because they could and it was interesting enough to warrant a journal
publication.

\section{Part 2: The Algorithm}

For those of us who study isomorphism problems, there are two camps of how to
solve them. Group theoretic methods are much better for worst-case applications.
But in practice combinatorial methods are much more efficient. The combinatorial
method used was called a canonical graph labelling, but is more commonly
referred to as naive colour refinement. It is called naive colour refinement
because it is 1 dimension of the popular weisfeiler leman algorithm for graph
isomorphism and is prevelant in almost all heuristic and practical algorithms. I
called their algorithm really really naive naive colour refinement because they
did not particularly care about how practically effective the algorithm was,
they were more interested in achieving a good bound for the algorithm
$O(n^{2})$. Since than many more algorithms have been analysed and improved upon
their initial result and at the time graph isomorphism was already a solved
problem practically, however the simplicity of the algorithm cannot be beaten.

This runs in linear time on a 1-step RAM Turing Machine operating on word size
$\log_{n}$.

\section{Part 3: The Analysis and The Probabilistic Method}

\section{Part 4: Linear Analogues}
