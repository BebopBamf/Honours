(TeX-add-style-hook
 "trp-literature-review"
 (lambda ()
   (LaTeX-add-bibitems
    "ArvindEtAl2017"
    "ArvindEtAl2017a"
    "Babai2016"
    "BabaiEtAl1980"
    "BrooksbankEtAl2019"
    "Chen1957"
    "DasEtAl2023"
    "DurEtAl2000"
    "GoldreichEtAl1991"
    "GrochowQiao2021"
    "GrochowQiao2023"
    "GrochowQiao2023a"
    "IvanyosQiao2019"
    "Karp1972"
    "LiQiao2017"
    "LiQiao2017a"
    "McKay1981"
    "McKayPiperno2014"
    "Miller1978"
    "OBrien1994"
    "PfefferEtAl2019"
    "Rosenbaum2013"
    "Sun2023"
    "Sun2023a"
    "TangEtAl2022"))
 '(or :bibtex :latex))

